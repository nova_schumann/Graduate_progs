	integer mx,my,m1,mxy,mmx,mmy,num
	parameter(mx=80,my=80)
	real ta(my,mx),s1(my,mx),s2(my,mx),pi,d(my,mx),a(my,mx)
	real m0,sum,length,wideth1,wideth2,ex,ey
	mxy=mx*my
	pi=4.*atan(1.)
	open(10,file='invo.data')
	open(20,file='1-80')
	open(30,file='time')
	open(40,file='invslp.in')
		read(40,*)
		read(40,*)
		read(40,*) num
	do i=1,num
	  read(40,*)
	end do
	read(40,*) mmx,mmy
	close(40)
	m1=1
	do 15 i=1,mmx
	  
	  read(10,'(8f10.5)')(ta(j,i),j=1,mmy)
	  
 15	continue

	m1=1
	do 20 i=1,mmx
	  
	  read(10,'(8e12.4)')(s1(j,i),j=1,mmy)
	  
 20	continue

	m1=1
	do 22 i=1,mmx
	  
	  read(10,'(8e12.4)')(s2(j,i),j=1,mmy)
	  
 22	continue
	close(10)
	
	do i=1,mmy
	  do j=1,mmx
! s1 along dip of fault plane
! s2 along strike of fault plane
	  if (s2(i,j)<0.) s2(i,j)=-s2(i,j)
	  if (s1(i,j)<0.) s1(i,j)=-s1(i,j)
!	  s2(i,j)=-s2(i,j)
!	  s1(i,j)=-s1(i,j)
	  d(i,j)=sqrt(s1(i,j)**2+s2(i,j)**2)
	  if(s2(i,j)==0..and.s1(i,j)==0.) then
	    a(i,j)=0.
	  else  if(s2(i,j)==0..and.s1(i,j)>0.) then
	    a(i,j)=0.
	  else  if(s2(i,j)==0..and.s1(i,j)<0.) then
	    a(i,j)=180.  
	  else  if(s2(i,j)>0..and.s1(i,j)==0.) then
	    a(i,j)=90.
	  else  if(s2(i,j)<0..and.s1(i,j)==0.) then
	    a(i,j)=270.  
	  else  if(s2(i,j)/=0..and.s1(i,j)/=0.) then  
	    a(i,j)=atan(s2(i,j)/s1(i,j))
	    a(i,j)=a(i,j)*180./pi
!            if(a(i,j).lt.0.) then
!              a(i,j)=90.+a(i,j)
!            end if
            if(s2(i,j)<0..and.s1(i,j)<0.) then
              a(i,j)=180.+a(i,j)
            end if  
	    	if(s2(i,j)<0..and.s1(i,j)>0.) then
              a(i,j)=a(i,j)
            end if
            if(s2(i,j)>0..and.s1(i,j)<0.) then
              a(i,j)=180.+a(i,j)
            end if
	  end if  
	 end do  
	end do  
	m1=1
	do i=1,mmy
	  
	  write(20,'(16(f10.2,1x,f6.1))') (d(i,j),a(i,j),j=1,mmx)
	  
	end do  
        sum=0.
        do i=1,mmy
          do j=1,mmx
            sum=sum+d(i,j)
          end do
        end do
        sum=sum/(mmx*mmy)
        open(99,file='invslp.in')
        read(99,*)
        read(99,*)
        read(99,*) length,wideth1,wideth2,ex,ey
        m0=3.*10**9*(length*(wideth2-wideth1)*10**6)*sum/100.
        print *,'M0=',m0
		open (31,file='seisMovement.txt')
		write(31,*) m0
	open(98,file='epi')
	write(98,*) ex,-ey
	
	m1=1
	do i=1,mmy
	  
	  write(30,'(16f6.2)') (ta(i,j),j=1,mmx)
	  
	end do  	
	
	close(20)
	close(30)
	close(98)
	close(99)
	end
	