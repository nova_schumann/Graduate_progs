c......................................................................
c             MAIN PROGRAM FOR STRONG MOTION SOURCE INVERSE
c
c    program : invstl.for
c    purpose : inverse for the slip intensity distribution from the 
c              strong motion seismograms.
c
c    input:
c        (1)   ite - number of iterations (not used in this program)
c              damp - damping parameter
c              rns - noise to signal ratio
c              di - correlation distance in width
c              dj - correlation distance in dip
c			 n - total number of points of the wavelet
c              twin - rise time
c              fl1 - low frequency corner of the bandpass
c              fh1 - high frequency corner of the bandpass
c              fl2 - low frequency transition cut off
c              fh2 - high frequency transition cut off
c              wid - width of the fault (this could be a misdefinition)
c              d11 - upper fault position along dip (depth/sin(dip))
c              d22 - lower fault position along dip (depth/sin(dip))
c              wso - hypocenter location along width
c			 dso - hypocentral position along dip (depth/cos(dip))
c              theta - fault dip
c              fi - fault rake (in rad)
c              nlay - number of layer including the half space
c              h - layer thickness
c              vp - P wave velocity
c              ap - P wave velocity gradience
c              vs - S wave velocity 
c              as - S wave velocity gradience
c              mx, my - number of grid lines along width and length
c
c        (2)   ta - rupture time distribution on the fault (mx*my)
c              s - slip intensity distribution on the fault (mx*my)
c                  dip component
c              s - slip intensity distribution on the fault (mx*my)
c                  strike component
c
c        (3)   xobs - station coordinate along strike from epicenter
c              yobs - station coordinate perpendicular to strike from
c                     epicenter
c              ns - total number of time series points
c              icomp - component index : 1 - along strike 
c                                        2 - perpendicular to strike
c                                        3 - vertical
c              anorm - ploting scale constance (not useful here)
c              dt - time step
c              stname - station abreviation
c              t - time series
c              seis - observation seismogram
c              
c    output:
c              ta - rupture time distribution on the fault (mx*my)
c              s - slip intensity distribution on the fault (mx*my)
c                  dip component
c              s - slip intensity distribution on the fault (mx*my)
c                  strike component
c              
c                                   Yuehua Zeng, June 6, 1993
c......................................................................
c
	parameter (mm1=800,mm2=250*50,mm3=800*50,mm4=800*50*50,
     +		   mm5=1301*800,mm6=50,mm7=800)
	dimension ta(mm1),d(2*mm1),d1(2*mm1),ak(2*mm1),t(mm2),seis(mm2),
     +		  syn(mm2),syni(mm2),n1(mm3),n2(mm3),n3(mm3),adt(mm4),
     +	          adti(mm4),ads(mm4),adsi(mm4),ar(mm5),a(mm5),b(mm5),
     +		  xobs(mm6),yobs(mm6),ns(mm6),icomp(mm6),x1(mm7),
     +		  x2(mm7),ihb(mm6),dh(2*mm1),anorm(mm6),s1(mm1),s2(mm1)
	real*8 ar,ak,d,d1,a,b,x1,x2,data,ainv,sigma,epsi(mm6)
	character*10 name
	common/model/wid,d11,d22,dso,theta,fi,nlay,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	common/coord/mx,my,sx(150),sy(150)
	common/init/aux(mm1),s(mm1)
	common/wvlet/wlet(-200:200),wilet(-200:200)
c
c  constant parameters
	pi=4.0*atan(1.0)
	sigma=1.0d1
	err=1.0e-6
c
c  read in source and media parameters
	open(10,file='invslp.in',form='formatted',status='old')
	read(10,'(i5,4f10.5)')iter,damp,rns,di,dj
	read(10,'(i5,5f10.5)')n,twin,fl1,fl2,fh1,fh2
	read(10,'(7f10.5)')wid,d11,d22,wso,dso,theta,fi
	read(10,'(i5)')nlay
	read(10,'(5f10.5)')(h(i),vp(i),ap(i),vs(i),as(i),i=1,nlay)
	read(10,'(2i5)')mx,my
	close(10)
	fi1=0.0
	if(fi.gt.0.5*pi)fi1=pi
c
c  read in observation data
	open(10,file='seis.dat',form='formatted',status='old')
	read(10,'(i5)')i1
	k1=0
	do 2 i=1,i1
	  read(10,'(2f10.5,2i5,2f10.5)')xobs(i),yobs(i),ns(i),icomp(i),
     +				       dt,anorm(i)
	  read(10,'(10e12.6)')(t(j),seis(j),j=k1+1,k1+ns(i))
	  do 1 j=k1+1,k1+ns(i)
	    seis(j)=seis(j)*anorm(i)
  1	  continue
	  k1=k1+ns(i)
  2	continue
	close(10)
c
c  Create the local source coordinates
	dx=wid/float(mx-1)
	sx(1)=-wso
	do 5 i=2,mx
	  sx(i)=dx+sx(i-1)
  5	continue
	dy=(d22-d11)/float(my-1)
	sy(1)=0.0
	do 10 i=2,my
	  sy(i)=dy+sy(i-1)
 10	continue
	dxy=dx*dy
	do 12 i=1,i1
	  anorm(i)=anorm(i)*dxy
 12	continue
c
c  Read in rupture time and slip density function distributions
	open(10,file='invo1.dat',form='formatted',status='old')
	m1=1
	do 15 i=1,mx
	  m2=m1+my-1
	  read(10,'(8f10.5)')(ta(j),j=m1,m2)
	  m1=m2+1
 15	continue
c
	m1=1
	do 20 i=1,mx
	  m2=m1+my-1
	  read(10,'(8e12.4)')(s1(j),j=m1,m2)
	  m1=m2+1
 20	continue
c
	m1=1
	do 22 i=1,mx
	  m2=m1+my-1
	  read(10,'(8e12.4)')(s2(j),j=m1,m2)
	  m1=m2+1
 22	continue
	close(10)
c
	tmin=ta(1)
	imin=1
	mxy=mx*my
	mxy2=2*mxy
	do 25 i=2,mxy
	  if(tmin.gt.ta(i))then
	    tmin=ta(i)
	    imin=i
	  end if
 25	continue
c
c  loop for calculating the synthesis and its derivatives with respect
c  to the model parameters.
	call wavelet(n,dt,twin,fl1,fl2,fh1,fh2)
	k1=1
	l1=1
	m1=1
	do 50 i=1,i1
	  do 30 j=1,mxy
	    aux(j)=ta(j)
	    s(j)=s1(j)
 30	  continue
	  fi=0.5*pi
c
	  call synth(xobs(i),yobs(i),nn,ns(i),t(k1),syn(k1),n1(l1),
     +		     n2(l1),n3(l1),adt(m1),adti(m1),ads(m1),adsi(m1),
     +	             icomp(i),ihb(i))
	  do 32 j=1,mxy
	    aux(j)=ta(j)
	    s(j)=s2(j)
 32	  continue
	  fi=fi1
c
	  call synth(xobs(i),yobs(i),nn,ns(i),t(k1),syni(k1),n1(l1),
     +		     n2(l1),n3(l1),a(m1),b(m1),adt(m1),adti(m1),
     +	             icomp(i),ihb(i))
	  print*,i
c
	  do 35 j=m1,m1+nn-1
	    adt(j)=adt(j)*anorm(i)
	    adti(j)=adti(j)*anorm(i)
	    ads(j)=ads(j)*anorm(i)
	    adsi(j)=adsi(j)*anorm(i)
 35	  continue
	  do 38 j=k1,k1+ns(i)-1
	    syn(j)=(syn(j)+syni(j))*anorm(i)
 38	  continue
c
	  l2=l1+mxy-1
	  k1=k1-1
	  m1=m1-1
	  do 40 j=l1,l2
	    n1(j)=n1(j)+k1
	    n3(j)=n3(j)+m1
 40	  continue
c
	  k1=k1+1+ns(i)
	  l1=l2+1
	  m1=m1+1+nn
 50	continue
c
	k1=k1-1
	l1=l1-1
	m1=m1-1
	print*,k1,l1,m1
c
c  Preparing the model covariance matrix
	open(60,file='for060.dat',form='formatted',status='unknown')
	ainv=0.0
	data=0.0
	epsi(1)=0.0
	do 80 i=1,mxy
	  d(i)=0.0
	  im=i+mxy
	  d(im)=0.0
	  k1=i
	  do 70 k=1,i1
	    do 60 l=n3(k1),n3(k1)+n2(k1)-1
	      d(i)=d(i)+ads(l)*ads(l)
	      d(im)=d(im)+adt(l)*adt(l)
 60	    continue
	    k1=k1+mxy
 70	  continue
c
	  if(ainv.lt.d(i))then
	    ainv=d(i)
	    write(60,'(i5,e12.4)')i,ainv
	  elseif(i.eq.imin)then
	    write(60,'(a4,i5,e12.4)')' ***',i,d(i)
	  end if
	  d(i)=dsqrt(d(i))
	  data=data+d(i)
	  d(im)=dsqrt(d(im))
	  epsi(1)=epsi(1)+d(im)
 80	continue
c
	data=data/float(mxy)
	epsi(1)=epsi(1)/float(mxy)
	do 90 i=1,mxy2
	  d(i)=1.0
 90	continue
c
	do 91 i=1,my
	  d(i)=0.0
	  d(mxy-i+1)=0.0
	  d(mxy+i)=0.0
	  d(mxy2-i+1)=0.0
 91	continue
	ij=my
	do 92 i=2,mx-1
	  d(ij+1)=1.0
	  d(ij+mxy+1)=1.0
	  ij=ij+my
	  d(ij)=0.0
	  d(ij+mxy)=0.0
 92	continue
	mmxy=mxy*(mxy+1)/2
	mmxy2=mxy2*(mxy2+1)/2
	call cova(ar,mmxy2,d,mxy2,mx,my,di,dj)
c
c  Estimate the relative noise variance for each seismogram
	m1=1
	l1=0
	do 230 ii=1,i1
c
c  calculate matrix A=G*Rm*Trans(G)
	  m2=m1+mxy-1
	  k1=mxy2*ns(ii)
	  do 100 i=1,k1
	    a(i)=0.0d0
100	  continue
c
	  j1=0
	  j2=mmxy
	  ni=0
	  do 140 i=m1,m2
	    ni=ni+1
	    k1=n3(i)
	    l2=n1(i)-l1
	    jk=mxy2*(l2-1)
	    do 130 k=l2,l2+n2(i)-1
c
	      ij=j1
	      do 110 j=1,ni
	        ij=ij+1
	        jk=jk+1
	        a(jk)=a(jk)+ar(ij)*ads(k1)
110	      continue
c
	      do 115 j=ni+1,mxy
	        ij=ij+j-1
	        jk=jk+1
	        a(jk)=a(jk)+ar(ij)*ads(k1)
115	      continue
c	      
	      jk=jk-mxy
	      ij=j2
	      do 120 j=1,ni+mxy
	        ij=ij+1
	        jk=jk+1
	        a(jk)=a(jk)+ar(ij)*adt(k1)
120	      continue
c
	      do 125 j=ni+mxy+1,mxy2
	        ij=ij+j-1
	        jk=jk+1
	        a(jk)=a(jk)+ar(ij)*adt(k1)
125	      continue
	      k1=k1+1
c
130	    continue
	    j1=j1+ni
	    j2=j2+ni+mxy
140	  continue
c
	  do 145 i=1,ns(ii)*(ns(ii)+1)/2
	    b(i)=0.0d0
145	  continue
	  ni=0
	  do 170 i=m1,m2
	    ni=ni+1
	    l2=n1(i)-l1
	    l=l2*(l2+1)/2
	    j1=(l2-2)*mxy2+ni
	    k1=n3(i)
	    do 160 j=l2,l2+n2(i)-1
	      jk=j1
	      ij=l
	      do 150 k=j,ns(ii)
	        jk=jk+mxy2
	        b(ij)=b(ij)+ads(k1)*a(jk)+adt(k1)*a(jk+mxy)
	        ij=ij+k
150	      continue
	      k1=k1+1
	      l=l+j+1
160	    continue
170	  continue
c
	  ij=0
	  do 182 i=1,ns(ii)
	    jk=0
	    do 176 j=1,ns(ii)
	      x1(j)=0.0
	      k1=jk
	      do 172 k=1,j
	        k1=k1+1
	        x1(j)=x1(j)+wlet(i-k)*b(k1)
172	      continue
	      do 174 k=j+1,ns(ii)
	        k1=k1+k-1
	        x1(j)=x1(j)+wlet(i-k)*b(k1)
174	      continue
	      jk=jk+j
176	    continue
	    do 180 j=1,i
	      ij=ij+1
	      a(ij)=0.0d0
	      do 178 k=1,ns(ii)
	        a(ij)=a(ij)+x1(k)*wlet(j-k)
178	      continue
180	    continue
182	  continue
c
c   calculate the maximum eigenvalue of matrix A
	  data=0.0
	  do 185 i=1,ns(ii)
	    x1(i)=1.0
185	  continue
c
	  do 220 i=1,1000
	    epsi(ii)=0.0
	    do 210 j=1,ns(ii)
	      x2(j)=0.0
	      k1=j*(j-1)/2
	      do 190 k=1,j
	        k1=k1+1
	        x2(j)=x2(j)+x1(k)*a(k1)
190	      continue
	      do 200 k=j+1,ns(ii)
	        k1=k1+k-1
	        x2(j)=x2(j)+x1(k)*a(k1)
200	      continue
	      epsi(ii)=epsi(ii)+x2(j)*x2(j)
210	    continue
c
	    epsi(ii)=dsqrt(epsi(ii))
	    if(abs(data-epsi(ii))/epsi(ii).lt.err)goto 225
	    data=epsi(ii)
c	    write(60,'(e24.16)')epsi(ii)
	    do 215 j=1,ns(ii)
	      x1(j)=x2(j)/epsi(ii)
215	    continue
220	  continue
225	  write(60,'(a18,e12.4)')' max eigenvalue : ',epsi(ii)
c
	  epsi(ii)=epsi(ii)*rns
	  m1=m2+1
	  l1=l1+ns(ii)
230	continue
c
c  Recursive mean square estimation for each additional seismogram
	do 235 i=1,mxy2
	  d(i)=0.0d0
235	continue
c
c  recursive estimation for each data point
	m1=1
	l1=0
	name(1:6)='00.dat'
	do 330 ii=1,i1
	  write(name(1:2),'(i2)')ii
c	  open(100,file=name(1:6),form='unformatted',status='unknown')
	  print*,ii
	  m2=m1+mxy-1
c
	  do 320 i=l1+1,l1+ns(ii)
	    do 240 j=1,mxy2
	      d1(j)=0.0d0
240	    continue
c
	    if(ihb(ii).eq.1)then
	      do 250 j=m1,m2
	        jk=j-m1+1
	        dh(jk)=0.0
		im=jk+mxy
	        dh(im)=0.0
	        l=i-n1(j)
	         do 245 k=0,n2(j)-1
	          dh(jk)=dh(jk)+wlet(l-k)*ads(n3(j)+k)
     +	                     +wilet(l-k)*adsi(n3(j)+k)
	          dh(im)=dh(im)+wlet(l-k)*adt(n3(j)+k)
     +	                     +wilet(l-k)*adti(n3(j)+k)
245	        continue
250	      continue
	    else
	      do 260 j=m1,m2
	        jk=j-m1+1
	        dh(jk)=0.0
		im=jk+mxy
	        dh(im)=0.0
	        l=i-n1(j)
	        do 255 k=0,n2(j)-1
	          dh(jk)=dh(jk)+wlet(l-k)*ads(n3(j)+k)
	          dh(im)=dh(im)+wlet(l-k)*adt(n3(j)+k)
255	        continue
260	      continue
	    end if
c
	    j1=0
	    do 275 j=1,mxy2
	      ij=j1
	      do 265 k=1,j
	        ij=ij+1
	        d1(j)=d1(j)+ar(ij)*dh(k)
265	      continue
	      do 270 k=j+1,mxy2
	        ij=ij+k-1
	        d1(j)=d1(j)+ar(ij)*dh(k)
270	      continue
	      j1=j1+j
275	    continue
c
	    ainv=epsi(ii)
	    data=seis(i)-syn(i)
	    do 280 j=1,mxy2
	      ainv=ainv+dh(j)*d1(j)
	      data=data-dh(j)*d(j)
280	    continue
	    ainv=1.0d0/ainv
	    do 290 j=1,mxy2
	      ak(j)=ainv*d1(j)
290	    continue
c
c  update the model perturbation and model covariance matrix
	    ij=0
	    do 310 j=1,mxy2
	      do 300 k=1,j
	        ij=ij+1
	        ar(ij)=ar(ij)-ak(k)*d1(j)
300	      continue
	      d(j)=d(j)+data*ak(j)
310	    continue
c	    write(100)(ak(j),j=1,mxy2)
c
320	  continue
	  m1=m2+1
	  l1=l1+ns(ii)
c	  close(100)
c
330	continue
	write(60,'(10e12.4)')(d(i),i=1,mxy2)
	do 340 i=1,mxy
	  s1(i)=s1(i)+d(i)
	  s2(i)=s2(i)+d(i+mxy)
c	  if(s1(i).lt.0.0)s1(i)=0.0
c	  if(s2(i).lt.0.0)s2(i)=0.0
c	  if(s1(i).lt.0.) then
c	   s1(i)=0.
c	   s2(i)=0.
c	  end if           
	  distance=sqrt(s1(i)**2+s2(i)**2)
	  if(distance.ge.1700) then
	    s1(i)=s1(i)-d(i)
	    s2(i)=s2(i)-d(i+mxy)
	  end if  
340	continue
c
c  dump out the inversion results
	open(10,file='invo1.data',form='formatted',status='unknown')
	m1=1
	do 350 i=1,mx
	  m2=m1+my-1
	  write(10,'(8f10.5)')(ta(j),j=m1,m2)
	  m1=m2+1
350	continue
	m1=1
	do 360 i=1,mx
	  m2=m1+my-1
	  write(10,'(8e12.4)')(s1(j),j=m1,m2)
	  m1=m2+1
360	continue
	m1=1
	do 365 i=1,mx
	  m2=m1+my-1
	  write(10,'(8e12.4)')(s2(j),j=m1,m2)
	  m1=m2+1
365	continue
	k1=1
	open(100,file='syn.dat')
	do i=1,i1
	  do j=k1,k1+ns(i)-1
	    write(100,'(10e12.6)') syn(j)
	  end do
	  k1=k1+ns(i)
	end do    
	print*,'finish!!'
	close(10)
	close(60)
	close(100)
c
	stop
	end
c
c ..........................................................
c
	subroutine cova(ar,mmxy2,d,mxy2,mx,my,di,dj)
c
c  purpose : create the covariance matrix for the model
	real*8 ar(mmxy2),d(mxy2)
	mxy=mx*my
	di=1./di
	dj=1./dj
c
	ij=0
	i1=0
	do 50 i=1,mx
	  do 40 j=1,my
	    i1=i1+1
	    j1=0
	    do 20 k=1,i-1
	      do 10 l=1,my
	        ij=ij+1
	        xi=float(k-i)*di
	        yj=float(l-j)*dj
	        j1=j1+1
	        ar(ij)=d(i1)*d(j1)*exp(-(xi*xi+yj*yj))
 10	      continue
 20	    continue
	    k=i
	    do 30 l=1,j-1
	      ij=ij+1
	      xi=float(k-i)*di
	      yj=float(l-j)*dj
	      j1=j1+1
	      ar(ij)=d(i1)*d(j1)*exp(-(xi*xi+yj*yj))
 30	    continue
	    ij=ij+1
c	    j1=j1+1
c	    ar(ij)=d(i1)*d(j1)
c	    print*,i1,j1,d(i1)
	    ar(ij)=d(i1)*d(i1)
 40	  continue
 50	continue
c
	do 110 i=1,mx
	  do 100 j=1,my
c
	    do 60 k=1,mxy
	      ij=ij+1
	      ar(ij)=0.0
 60	    continue
c
	    i1=i1+1
	    j1=mxy
	    do 80 k=1,i-1
	      do 70 l=1,my
	        ij=ij+1
	        xi=float(k-i)*di
	        yj=float(l-j)*dj
	        j1=j1+1
	        ar(ij)=d(i1)*d(j1)*exp(-(xi*xi+yj*yj))
 70	      continue
 80	    continue
	    k=i
	    do 90 l=1,j-1
	      ij=ij+1
	      xi=float(k-i)*di
	      yj=float(l-j)*dj
	      j1=j1+1
	      ar(ij)=d(i1)*d(j1)*exp(-(xi*xi+yj*yj))
 90	    continue
	    ij=ij+1
	    ar(ij)=d(i1)*d(i1)
100	  continue
110	continue
c
	return
	end
c
c.............................................................
c
	subroutine wavelet(n,dt,twin,fl1,fl2,fh1,fh2)
c.............................................................
c    input:      n -- total number of points of the wavelet
c               dt -- time interval
c             twin -- rise time
c              fl1 -- low frequency corner of the bandpass
c              fh1 -- high frequency corner of the bandpass
c              fl2 -- low frequency transition cut off
c              fh2 -- high frequency transition cut off
c    output:  wlet -- bandpassed wavelet
c            wilet -- Hilbert transform of wlet
c............................................................
c
	dimension hb(-400:400)
	complex v(1024),cz
	common/wvlet/wlet(-200:200),wilet(-200:200)
c
	pi=3.1415926
	pi2=0.5*pi
	cz=cmplx(0.0,0.0)
c
	ndwi=ifix(twin/dt)
	t=0.0
	do 10 i=1,n
	  if(i.gt.ndwi+1)then
	    wlet(i)=0.0
	    goto 10
	  end if
	  if(t.gt.dt)then
	    wlet(i)=sqrt(t+dt)-sqrt(t-dt)
	  else
	    wlet(i)=sqrt(t+dt)
	  end if
	  t=t+dt
 10	continue
c
c  extending the length of the seismogram to the power of 2
	if(n.le.512)then
	  m=512
	elseif(n.le.1024)then
	  m=1024
	end if
c
c  calculate dw and the Niquist frequency (Hz)
	dw=1.0/(float(m)*dt)
	ml1=ifix(fl1/dw)
	ml2=ifix(fl2/dw)
	mh1=ifix(fh1/dw)
	mh2=ifix(fh2/dw)
	fniq=1.0/(2.0*dt)
	m2=m/2+1
c
c  calculate the FFT of the seismogram
	do 20 i=1,n
	  v(i)=cmplx(wlet(i),0.0)
 20	continue
	do 30 i=n+1,m
	  v(i)=cz
 30	continue
	call fork(m,v,-1.)
c
c  bandpass the seismogram and make the inverse FFT
	do 40 i=1,ml1
	  v(i)=cz
 40	continue
	v(1)=cz
	band=-1.0
	db=2./float(ml2-ml1)
	do 50 i=ml1+1,ml2
	  band=band+db
	  v(i)=v(i)*(0.5+0.5*sin(pi2*band))
 50	continue
	band=-1.0
	db=2./float(mh2-mh1)
	do 55 i=mh1+1,mh2
	  band=band+db
	  v(i)=v(i)*(0.5-0.5*sin(pi2*band))
 55	continue
	do 60 i=mh2+1,m2
	  v(i)=cz
 60	continue
	k=0
	do 70 i=m2+1,m
	  k=k+2
	  v(i)=cmplx(real(v(i-k)),-aimag(v(i-k)))
 70	continue
	call fork(m,v,1.)
c
c  obtain the time domain wavelet in noncausal form
	do 80 i=-n,-1
	  wlet(i)=real(v(m+i+1))
 80	continue
	do 90 i=0,n
	  wlet(i)=real(v(i+1))
 90	continue
c
c  compute the Hilbert transform of the wavelet
	hb(0)=0.0
	hb(1)=-2.0*alog(2.0)/pi
	hb(-1)=-hb(1)
	do 100 i=2,400
	  dd=float(i)
	  hb(i)=-((1.+dd)*alog(1.+1./dd)+(1.-dd)*alog(1.-1./(1.-dd)))/pi
	  hb(-i)=-hb(i)
100	continue
c
	do 110 i=-n,n
	  wilet(i)=0.0
	  do 105 j=-n,n
	    wilet(i)=wilet(i)+wlet(j)*hb(j-i)
105	  continue
110	continue
c
	return
	end
c
c .....................................................................
c
	subroutine synth(xobs,yobs,nn,n,t,syn,nc1,nc2,nc3,adt,adti,
     +			 ads,adsi,icomp,ihb)
c
	parameter (mn=800,mn2=2*mn,ms=150,nt=300,mt=50000)
	dimension g(mn),t(nt),xt(mt),yt(mt),n1(mn2),n2(mn2),nc1(mn2),
     +		  nc2(mn2),nc3(mn2),adt(mt),ads(mt),syn(nt),adti(mt),
     +	          adsi(mt),syni(nt),synr(nt)
	logical*1 notex,notey
	common/model/wid,d11,d22,dso,theta,fi,nlay,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	common/coord/mx,my,sx(ms),sy(ms)
	common/init/ta(mn),s(mn)
	common/wvlet/wlet(-200:200),wilet(-200:200)
	common/value/t1,t3,g1,g3,s1,s3,t21,t23,t213,t233,at31,a2,b2,x1,
     +	             x2,y1,y2
	complex ads1,ads2,ads3,adt1,adt2,adt3,syn1,a2,g1,g2,g3,g
c	real*8 ads1,ads2,ads3,adt1,adt2,adt3,at31,t213,t223,a2,b2,syn1
c
	err=0.00001
	mxy=mx*my
	call tracei(xobs,yobs,ta,g,mxy,icomp)
	call conto(n,t,xt,yt,n1,n2,ta,mxy,mx,my)
c
c  calculate the derivatives of the seismograms with respect to t and s
	do 10 i=1,mxy
	  nc1(i)=0
	  nc2(i)=0
 10	continue
c
	ij=0
	ij2=0
	notex=.true.
	do 30 i=1,mx-1
	  notey=notex
	  do 20 j=1,my-1
	    ij=ij+1
	    ij2=ij2+2
	    ij1=ij2-1
	    if(notey)then
	      nmin=min0(n1(ij1),n1(ij2))
	      nmax=max0(n2(ij1),n2(ij2))
c
	      if(nmin.lt.nc1(ij).or.nc1(ij).eq.0)nc1(ij)=nmin
	      if(nmax.gt.nc2(ij))nc2(ij)=nmax
c
	      ija=ij+1
	      if(n1(ij1).lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=n1(ij1)
	      if(n2(ij1).gt.nc2(ija))nc2(ija)=n2(ij1)
c
	      ija=ij+my
	      if(n1(ij2).lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=n1(ij2)
	      if(n2(ij2).gt.nc2(ija))nc2(ija)=n2(ij2)
c
	      ija=ija+1
	      if(nmin.lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=nmin
	      if(nmax.gt.nc2(ija))nc2(ija)=nmax
c
	    else
	      nmin=min0(n1(ij1),n1(ij2))
	      nmax=max0(n2(ij1),n2(ij2))
c
	      if(n1(ij1).lt.nc1(ij).or.nc1(ij).eq.0)nc1(ij)=n1(ij1)
	      if(n2(ij1).gt.nc2(ij))nc2(ij)=n2(ij1)
c
	      ija=ij+1
	      if(nmin.lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=nmin
	      if(nmax.gt.nc2(ija))nc2(ija)=nmax
c
	      ija=ij+my
	      if(nmin.lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=nmin
	      if(nmax.gt.nc2(ija))nc2(ija)=nmax
c
	      ija=ija+1
	      if(n1(ij2).lt.nc1(ija).or.nc1(ija).eq.0)nc1(ija)=n1(ij2)
	      if(n2(ij2).gt.nc2(ija))nc2(ija)=n2(ij2)
c
	    end if
	    notey=.not.notey
 20	  continue
	  ij=ij+1
	  notex=.not.notex
 30	continue
c
	nn=1
	do 40 i=1,mxy
	  nc2(i)=nc2(i)-nc1(i)+1
	  nc3(i)=nn
	  nn=nn+nc2(i)
 40	continue
c
	nn=nn-1
	do 50 i=1,nn
	  adt(i)=0.0
	  adti(i)=0.0
	  ads(i)=0.0
	  adsi(i)=0.0
 50	continue
c
	do 60 i=1,n
	  synr(i)=0.0
	  syni(i)=0.0
 60	continue
c
	n3=0
	ij=0
	ij2=0
	notex=.true.
	do 100 i=1,mx-1
	  notey=notex
	  do 90 j=1,my-1
	    ij=ij+1
	    ij2=ij2+2
	    ij1=ij2-1
c
	    m11=ij
	    m12=m11+1
	    m21=m11+my
	    m22=m21+1
c
	    if(notey)then
	      l1=m12
	      l2=m22
	      l3=m11
	      l4=1
	      l5=1
 65	      if(abs(ta(l2)-ta(l1)).gt.abs(ta(l3)-ta(l1)))then
		ija=l2
	        l2=l3
	        l3=ija
		l4=0
	      end if
c
	      t1=ta(l1)
	      t2=ta(l2)
	      t3=ta(l3)
	      g1=g(l1)
	      g2=g(l2)-g1
	      g3=g(l3)-g1
	      s1=s(l1)
	      s2=s(l2)-s1
	      s3=s(l3)-s1
c
	      at31=1./(t3-t1)
	      t21=t2-t1
	      t213=t21*at31
	      t23=t2-t3
	      t233=t23*at31
	      a2=g2-g3*t213
	      b2=s2-s3*t213
c
	      do 70 k=n1(ij1),n2(ij1)
		if(l4.eq.1)then
		  n3=n3+2
		  x1=xt(n3-1)
		  x2=xt(n3)
		  y1=yt(n3-1)
		  y2=yt(n3)
		else
		  n3=n3+2
		  x1=yt(n3-1)
		  x2=yt(n3)
		  y1=xt(n3-1)
		  y2=xt(n3)
		end if
c
	        call deriv(t(k),syn1,ads1,ads2,ads3,adt1,adt2,adt3)
c
	        c=sign(at31,x2-x1)
	        synr(k)=synr(k)+c*real(syn1)
	        syni(k)=syni(k)+c*aimag(syn1)
		ija=nc3(l1)+k-nc1(l1)
		ads(ija)=ads(ija)+c*real(ads1)
		adsi(ija)=adsi(ija)+c*aimag(ads1)
		adt(ija)=adt(ija)+c*real(adt1)
		adti(ija)=adti(ija)+c*aimag(adt1)
		ija=nc3(l2)+k-nc1(l2)
		ads(ija)=ads(ija)+c*real(ads2)
		adsi(ija)=adsi(ija)+c*aimag(ads2)
		adt(ija)=adt(ija)+c*real(adt2)
		adti(ija)=adti(ija)+c*aimag(adt2)
		ija=nc3(l3)+k-nc1(l3)
		ads(ija)=ads(ija)+c*real(ads3)
		adsi(ija)=adsi(ija)+c*aimag(ads3)
		adt(ija)=adt(ija)+c*real(adt3)
		adti(ija)=adti(ija)+c*aimag(adt3)
c
 70	      continue
c
	      if(l5.eq.1)then
	        l1=m21
	        l2=m11
	        l3=m22
	        l4=1
	        l5=0
		ij1=ij2
	        goto 65
	      end if
c
	    else
	      l1=m11
	      l2=m21
	      l3=m12
	      l4=1
	      l5=1
 75	      if(abs(ta(l2)-ta(l1)).gt.abs(ta(l3)-ta(l1)))then
		ija=l2
	        l2=l3
	        l3=ija
		l4=0
	      end if
c
	      t1=ta(l1)
	      t2=ta(l2)
	      t3=ta(l3)
	      g1=g(l1)
	      g2=g(l2)-g1
	      g3=g(l3)-g1
	      s1=s(l1)
	      s2=s(l2)-s1
	      s3=s(l3)-s1
c
	      at31=1./(t3-t1)
	      t21=t2-t1
	      t213=t21*at31
	      t23=t2-t3
	      t233=t23*at31
	      a2=g2-g3*t213
	      b2=s2-s3*t213
c
	      do 80 k=n1(ij1),n2(ij1)
		if(l4.eq.1)then
		  n3=n3+2
		  x1=xt(n3-1)
		  x2=xt(n3)
		  y1=yt(n3-1)
		  y2=yt(n3)
		else
		  n3=n3+2
		  x1=yt(n3-1)
		  x2=yt(n3)
		  y1=xt(n3-1)
		  y2=xt(n3)
		end if
c
	        call deriv(t(k),syn1,ads1,ads2,ads3,adt1,adt2,adt3)
c
	        c=sign(at31,x2-x1)
	        synr(k)=synr(k)+c*real(syn1)
	        syni(k)=syni(k)+c*aimag(syn1)
		ija=nc3(l1)+k-nc1(l1)
		ads(ija)=ads(ija)+c*real(ads1)
		adsi(ija)=adsi(ija)+c*aimag(ads1)
		adt(ija)=adt(ija)+c*real(adt1)
		adti(ija)=adti(ija)+c*aimag(adt1)
		ija=nc3(l2)+k-nc1(l2)
		ads(ija)=ads(ija)+c*real(ads2)
		adsi(ija)=adsi(ija)+c*aimag(ads2)
		adt(ija)=adt(ija)+c*real(adt2)
		adti(ija)=adti(ija)+c*aimag(adt2)
		ija=nc3(l3)+k-nc1(l3)
		ads(ija)=ads(ija)+c*real(ads3)
		adsi(ija)=adsi(ija)+c*aimag(ads3)
		adt(ija)=adt(ija)+c*real(adt3)
		adti(ija)=adti(ija)+c*aimag(adt3)
c
 80	      continue
c
	      if(l5.eq.1)then
	        l1=m22
	        l2=m12
	        l3=m21
	        l4=1
	        l5=0
		ij1=ij2
	        goto 75
	      end if
c
	    end if
c
	    notey=.not.notey
 90	  continue
	  ij=ij+1
	  notex=.not.notex
100	continue
c
c  convolution with the wavelet
	smax=0.0
	do 110 i=1,n
	  if(smax.lt.abs(synr(i)))smax=abs(synr(i))
110	continue
	smax=smax*err
	do 115 i=1,n
	  if(smax.lt.abs(syni(i)))goto 130
115	continue
	ihb=0
	do 125 i=1,n
	  syn(i)=0.0
	  do 120 j=1,n
	    syn(i)=syn(i)+synr(j)*wlet(i-j)
120	  continue
125	continue
	return
130	ihb=1
	do 140 i=1,n
	  syn(i)=0.0
	  do 135 j=1,n
	    syn(i)=syn(i)+synr(j)*wlet(i-j)+syni(j)*wilet(i-j)
135	  continue
140	continue
c
	return
	end
c
c .................
c
	subroutine deriv(t,syn,ads1,ads2,ads3,adt1,adt2,adt3)
	common/value/t1,t3,g1,g3,s1,s3,t21,t23,t213,t233,at31,a2,b2,x1,
     +	             x2,y1,y2
	complex ads1,ads2,ads3,adt1,adt2,adt3,syn,a1,a2,aa1,aa2,g1,g3
c	real*8 at31,t213,t013,t033,t223,a1,a2,b1,b2,x,xx,xxx,
c     +	       aa1,aa2,bb1,bb2,syn
c
	t013=(t-t1)*at31
	t033=(t-t3)*at31
	a1=g1+g3*t013
	b1=s1+s3*t013
	x12=x1*x1
	x22=x2*x2
	x=x2-x1
	xx=(x22-x12)*0.5
	xxx=(x22*x2-x12*x1)/3.
c
	aa1=a1*x+a2*xx
	aa2=a1*xx+a2*xxx
	syn=aa1*b1+aa2*b2
c
	ads1=aa1*(1.-t013)+aa2*(t213-1.)
	ads2=aa2
	ads3=aa1*t013-aa2*t213
c
	adt1=syn+s3*(aa1*t033-aa2*t233)
	adt2=-s3*aa2
	adt3=-syn-s3*ads3
c
	bb1=b1*x+b2*xx
	bb2=b1*xx+b2*xxx
	adt1=(adt1+g3*(bb1*t033-bb2*t233))*at31
	adt2=(adt2-g3*bb2)*at31
	adt3=(adt3-g3*(bb1*t013-bb2*t213))*at31
c
	call xdt(x2,y2,t21,t23,x,xx,xxx)
	aa1=(a1+a2*x2)*(b1+b2*x2)
	adt1=adt1+aa1*x
	adt2=adt2+aa1*xx
	adt3=adt3+aa1*xxx
c
	call xdt(x1,y1,t21,t23,x,xx,xxx)
	aa1=(a1+a2*x1)*(b1+b2*x1)
	adt1=adt1-aa1*x
	adt2=adt2-aa1*xx
	adt3=adt3-aa1*xxx
c
	return
	end
c
c ..................
c
	subroutine xdt(x,y,t21,t23,dxdt1,dxdt2,dxdt3)
	logical*1 note
c	real*8 dxdt1,dxdt2,dxdt3
c
	note=(x.eq.1.0).and.(abs(t21).ge.abs(t23))
	if(x.eq.0.0)then
	  dxdt1=0.0
	  dxdt2=0.0
	  dxdt3=0.0
	elseif(y.eq.0.0.and.x.ne.1.0.or.note)then
	  dxdt1=1.0/t21
	  dxdt2=-x*dxdt1
	  dxdt1=-dxdt1-dxdt2
	  dxdt3=0.0
	else
	  dxdt1=0.0
	  dxdt3=1.0/t23
	  dxdt2=-x*dxdt3
	  dxdt3=-dxdt3-dxdt2
	end if
	return
	end
c
c...................
c
	subroutine conto(n,t,xt,yt,n1,n2,ta,mxy,mx,my)
c
	parameter (mt=50000)
	dimension ta(mxy),t(n),n1(2*mxy),n2(2*mxy),xt(mt),yt(mt)
	logical*1 notex,notey
c
c  Find the positions of the contour values at each grid
	k1=1
	ij=0
	ij2=0
	notex=.true.
	nn=0
	do 220 i=1,mx-1
	  notey=notex
	  do 210 j=1,my-1
	    ij=ij+1
	    ij2=ij2+2
	    ij1=ij2-1
	    t11=ta(ij)
	    t12=ta(ij+1)
	    t21=ta(ij+my)
	    t22=ta(ij+my+1)
	    if(notey)then
	      t1=t12
	      t2=t22
	      t3=t11
	      tmax=amax1(t1,t2,t3)
	      tmin=amin1(t1,t2,t3)
c
	      if(k1.gt.n)k1=n
	      if(t(k1).lt.tmin)then
	        do 40 k=k1+1,n
	          if(tmin.lt.t(k))goto 45
 40	        continue
 45	        k1=k
	      else
	        do 50 k=k1,1,-1
	          if(t(k).le.tmin)goto 55
 50	        continue
 55	        k1=k+1
	      end if
	      do 60 k=k1,n
	        if(tmax.le.t(k))goto 65
 60	      continue
 65	      k2=k-1
c
	      n1(ij1)=k1
	      n2(ij1)=k2
c
	      if(k1.le.1)goto 68
	      if(t1.eq.t(k1-1).and.t2.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n1(ij1)=k1-1
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t1.eq.t(k1-1).and.t3.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
	        n1(ij1)=k1-1
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t3.eq.t(k1-1).and.t2.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n1(ij1)=k1-1
	        n2(ij1)=n2(ij1)+1
	      end if
c
 68	      do 70 k=k1,k2
	        t1k=t1-t(k)
	        t2k=t2-t(k)
	        t3k=t3-t(k)
	        np=0
	        if(t1k*t2k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t1k/(t1-t2)
	          yt(nn)=0.0
	        end if
	        if(t1k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=0.0
	          yt(nn)=t1k/(t1-t3)
		  if(np.eq.2)goto 70
	        end if
	        if(t2k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t3k/(t3-t2)
	          yt(nn)=t2k/(t2-t3)
		  if(np.eq.2)goto 70
	        end if
	        if(t1k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 70
		end if
	        if(t2k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=1.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 70
		end if
	        if(t3k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=1.0
		  if(np.eq.2)goto 70
		end if
	        write(6,'(a31)')' Error in the data or program !'
		stop
 70	      continue
c
	      if(k2.ge.n)goto 75
	      if(t1.eq.t(k2+1).and.t2.eq.t(k2+1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t1.eq.t(k2+1).and.t3.eq.t(k2+1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t3.eq.t(k2+1).and.t2.eq.t(k2+1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n2(ij1)=n2(ij1)+1
	      end if
c
 75	      t1=t21
	      t2=t11
	      t3=t22
	      tmax=amax1(t1,t2,t3)
	      tmin=amin1(t1,t2,t3)
c
	      if(k1.gt.n)k1=n
	      if(t(k1).lt.tmin)then
	        do 80 k=k1+1,n
	          if(tmin.lt.t(k))goto 85
 80	        continue
 85	        k1=k
	      else
	        do 90 k=k1,1,-1
	          if(t(k).le.tmin)goto 95
 90	        continue
 95	        k1=k+1
	      end if
	      do 100 k=k1,n
	        if(tmax.le.t(k))goto 105
100	      continue
105	      k2=k-1
c
	      n1(ij2)=k1
	      n2(ij2)=k2
c
	      do 110 k=k1,k2
	        t1k=t1-t(k)
	        t2k=t2-t(k)
	        t3k=t3-t(k)
	        np=0
	        if(t1k*t2k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t1k/(t1-t2)
	          yt(nn)=0.0
	        end if
	        if(t1k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=0.0
	          yt(nn)=t1k/(t1-t3)
		  if(np.eq.2)goto 110
	        end if
	        if(t2k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t3k/(t3-t2)
	          yt(nn)=t2k/(t2-t3)
		  if(np.eq.2)goto 110
	        end if
	        if(t1k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 110
		end if
	        if(t2k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=1.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 110
		end if
	        if(t3k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=1.0
		  if(np.eq.2)goto 110
		end if
	        write(6,'(a31)')' Error in the data or program !'
		stop
110	      continue
c
	    else
	      t1=t11
	      t2=t21
	      t3=t12
	      tmax=amax1(t1,t2,t3)
	      tmin=amin1(t1,t2,t3)
c
	      if(k1.gt.n)k1=n
	      if(t(k1).lt.tmin)then
	        do 120 k=k1+1,n
	          if(tmin.lt.t(k))goto 125
120	        continue
125	        k1=k
	      else
	        do 130 k=k1,1,-1
	          if(t(k).le.tmin)goto 135
130	        continue
135	        k1=k+1
	      end if
	      do 140 k=k1,n
	        if(tmax.le.t(k))goto 145
140	      continue
145	      k2=k-1
c
	      n1(ij1)=k1
	      n2(ij1)=k2
c
	      if(k1.le.1)goto 148
	      if(t1.eq.t(k1-1).and.t3.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
	        n1(ij1)=k1-1
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t3.eq.t(k1-1).and.t2.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n1(ij1)=k1-1
	        n2(ij1)=n2(ij1)+1
	      end if
c
148	      do 150 k=k1,k2
	        t1k=t1-t(k)
	        t2k=t2-t(k)
	        t3k=t3-t(k)
	        np=0
	        if(t1k*t2k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t1k/(t1-t2)
	          yt(nn)=0.0
	        end if
	        if(t1k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=0.0
	          yt(nn)=t1k/(t1-t3)
		  if(np.eq.2)goto 150
	        end if
	        if(t2k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t3k/(t3-t2)
	          yt(nn)=t2k/(t2-t3)
		  if(np.eq.2)goto 150
	        end if
	        if(t1k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 150
		end if
	        if(t2k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=1.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 150
		end if
	        if(t3k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=1.0
		  if(np.eq.2)goto 150
		end if
	        write(6,'(a31)')' Error in the data or program !'
		stop
150	      continue
c
	      if(k2.ge.n)goto 155
	      if(t1.eq.t(k2+1).and.t3.eq.t(k2+1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
	        n2(ij1)=n2(ij1)+1
	      end if
	      if(t3.eq.t(k2+1).and.t2.eq.t(k2+1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=1.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n2(ij1)=n2(ij1)+1
	      end if
c
155	      t1=t22
	      t2=t12
	      t3=t21
	      tmax=amax1(t1,t2,t3)
	      tmin=amin1(t1,t2,t3)
c
	      if(k1.gt.n)k1=n
	      if(t(k1).lt.tmin)then
	        do 160 k=k1+1,n
	          if(tmin.lt.t(k))goto 165
160	        continue
165	        k1=k
	      else
	        do 170 k=k1,1,-1
	          if(t(k).le.tmin)goto 175
170	        continue
175	        k1=k+1
	      end if
	      do 180 k=k1,n
	        if(tmax.le.t(k))goto 185
180	      continue
185	      k2=k-1
c
	      n1(ij2)=k1
	      n2(ij2)=k2
c
	      if(k1.le.1)goto 188
	      if(t1.eq.t(k1-1).and.t2.eq.t(k1-1))then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n1(ij2)=k1-1
	        n2(ij2)=n2(ij2)+1
	      end if
188	      do 190 k=k1,k2
	        t1k=t1-t(k)
	        t2k=t2-t(k)
	        t3k=t3-t(k)
	        np=0
	        if(t1k*t2k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t1k/(t1-t2)
	          yt(nn)=0.0
	        end if
	        if(t1k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=0.0
	          yt(nn)=t1k/(t1-t3)
		  if(np.eq.2)goto 190
	        end if
	        if(t2k*t3k.lt.0.0)then
	          np=np+1
	          nn=nn+1
	          xt(nn)=t3k/(t3-t2)
	          yt(nn)=t2k/(t2-t3)
		  if(np.eq.2)goto 190
	        end if
	        if(t1k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 190
		end if
	        if(t2k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=1.0
	 	  yt(nn)=0.0
		  if(np.eq.2)goto 190
		end if
	        if(t3k.eq.0.0)then
		  np=np+1
		  nn=nn+1
		  xt(nn)=0.0
	 	  yt(nn)=1.0
		  if(np.eq.2)goto 190
		end if
	        write(6,'(a31)')' Error in the data or program !'
		stop
190	      continue
	      if(t1.eq.t(k2+1).and.t2.eq.t(k2+1).and.k2.ge.n)then
		nn=nn+1
	        xt(nn)=0.0
	        yt(nn)=0.0
		nn=nn+1
	        xt(nn)=1.0
	        yt(nn)=0.0
	        n2(ij2)=n2(ij2)+1
	      end if
c
	    end if
	    notey=.not.notey
210	  continue
	  ij=ij+1
	  notex=.not.notex
220	continue
c
	return
	end
c
c.......................
c
	subroutine tracei(xobs,yobs,ta,g,mxy,icomp)
C
C----------------------------------------------------------------------C
C    This program creates the source coordinate and calculates ray     C
C    tracing in a 3-D vertical inhomogeneous media for an arbitrary    C
C    rupture source.                                                   C
C                                                                      C
C                       Written by Yuehue Zeng at USC, sept. 11, 1988  C
C----------------------------------------------------------------------C
C               
	parameter (ms=150)
c
	dimension ta(mxy),g(mxy),sy(ms),sz(ms)
	common/model/wid,d1,d2,dso,theta,fi,n,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	common/coord/mx,my,sx(ms),syy(ms)
	complex bx,by,bz,coef1,coef2,g,cs
	double precision p,p1,p2,pn,aux,aux1,aux2
	pi=3.1415926
	ds=(d2-d1)/float(my-1)
	err=0.01*ds
c
c  Create global source coordinate, and calculate fault 
c  normal and slip vector
	do 10 i = 1,n-2
	  ap(i)=(vp(i+1)-vp(i))/(h(i+1)-h(i))
	  as(i)=(vs(i+1)-vs(i))/(h(i+1)-h(i))
 10	continue
	dy=ds*cos(theta)
	dz=ds*sin(theta)
	sy(1)=-(dso-d1)*cos(theta)
	sz(1)=d1*sin(theta)
	do 100 i=2,my
	  sy(i)=dy+sy(i-1)
	  sz(i)=dz+sz(i-1)
100	continue
c
	vectn2=sin(theta)
	vectn3=-cos(theta)
	vectm1=cos(fi)
	vectm2=-cos(theta)*sin(fi)
	vectm3=-sin(theta)*sin(fi)
c
c  Ray tracing
	m1=1
c	mj=-1
	mj=1
	x=sx(1)-xobs
	y=sy(my)-yobs
	r=sqrt(x*x+y*y+sz(my)*sz(my))
	p=0.5*sqrt(r*r-sz(my)*sz(my))/(r*vs(1))
c
c  Loop for the two points ray tracing betweem observation and
c  all the source points 
	do 140 i=1,mx
	  m2=m1+my-1
	  mj=-mj
	  if(mj.eq.1)then
	    m11=m1
	    m22=m2
	  elseif(mj.eq.-1)then
	    m11=m2
	    m22=m1
	  end if
	  x=sx(i)-xobs
	  xx=x*x
	  do 130 j=m11,m22,mj
            l=j-m1+1
	    y=sy(l)-yobs
	    r=sqrt(xx+y*y)
c
c  Calculate fi0
	    if(r.eq.0.0)then
	      fi0=0.5*pi
	    else
	      fi0=atan2(y,x)
	    end if
c
	    do 110 k=1,n
	      if(h(k).gt.sz(l)) goto 1
110	    continue
  1         i1=k-1
c
c  Iteration for the two points ray tracing
	    ite=-1
	    nc=0
	    z1=h(n)
	    z2=h(1)
  2	    call ray1(p,zd,zp,sz(l),i1,r0,rp,r,1,nc,aux,pn)
	    ite=ite+1
	    if(zd.gt.sz(l))then
	      p2=p
	      aux2=aux
	      z2=zd
	    else
	      p1=p
	      aux1=aux
	      z1=zd
	    end if
	    if(ite.gt.10)then
	      if(z2.gt.sz(l).and.z1.lt.sz(l))goto 3
	      if(z2.lt.sz(l))then
	        p=p-0.00001
	        goto 2
	      else
	        p=p+0.00001
	        goto 2
	      end if
	    end if
	    if(p.ge.0.0003)then
	      delta=sz(l)-zd
	    else
	      delta=r-r0
	      zp=rp
	    end if
	    if(abs(delta).lt.err)then
	      call ray1(p,zd,zp,sz(l),i1,t,rp,r,2,nc,aux,pn)
	      goto 5
	    endif
	    tmp=delta/zp
	    p=p+tmp
	    if(p.le.0.0)p=(p-tmp)/2.0
	    goto 2
  3	    if(zd.gt.sz(l))then
	      p2=p
	      aux2=aux
	      z2=zd
	    else
	      p1=p
	      aux1=aux
	      z1=zd
	    end if
	    p=0.5d0*(p1+p2)
  4	    call ray0(p,zd,sz(l),i1,r,1,nc,aux)
	    ite=ite+1
	    delta=sz(l)-zd
	    if(abs(delta).lt.err)then
	      call ray1(p,zd,zp,sz(l),i1,t,rp,r,2,nc,aux,pn)
	      goto 5
	    endif
	    if(zd.gt.sz(l))then
	      p2=p
	      aux2=aux
	      z2=zd
	    else
	      p1=p
	      aux1=aux
	      z1=zd
	    end if
	    if(ite.gt.40.and.(z2-z1).lt.ds)then
	      call ray1(p1,zd,zp1,sz(l),i1,t1,rp,r,2,nc,aux1,pn)
	      call ray1(p2,zd,zp2,sz(l),i1,t2,rp,r,2,nc,aux2,pn)
	      aux1=z1/(z1+z2)
	      aux2=z2/(z1+z2)
	      p=p1*aux2+p2*aux1
	      zp=zp1*aux2+zp2*aux1
	      t=t1*aux2+t2*aux1
	      goto 5
	    endif
	    p=0.5d0*(p1+p2)
	    goto 4
c
  5	    theta0=asin(p*vs(1))
	    call Greens(theta0,fi0,bx,by,bz,cx,cy)
c
c  Calculate the transmission coeficients
	    rmod1=1.
	    rph1=0.
	    rmod2=1.
	    rph2=0.
	    do 120 ii=1,i1-1
	      hi=h(ii+1)-h(ii)
	      vp1=vp(ii)+ap(ii)*hi
	      vs1=vs(ii)+as(ii)*hi
	      ro1=1.7+0.2*vp1
	      vp2=vp(ii+1)
	      vs2=vs(ii+1)
	      ro2=1.7+0.2*vp2
	      call coefps(p,vp1,vs1,ro1,vp2,vs2,ro2,8,rm1,rp1)
	      call coefsh(p,vp1,vs1,ro1,vp2,vs2,ro2,2,rm2,rp2)
	      rmod1=rmod1*rm1
	      rph1=rph1+rp1
	      rmod2=rmod2*rm2
	      rph2=rph2+rp2
120	    continue
	    coef1=rmod1*cmplx(cos(rph1),sin(rph1))
	    coef2=rmod2*cmplx(cos(rph2),sin(rph2))
c
c  Calculate vectors of ray tangent and normals
	    hi=sz(l)-h(i1)
	    ro1=1.7+0.2*(vp(i1)+ap(i1)*hi)
	    ro0=1.7+0.2*vp(1)
	    vs1=vs(i1)+as(i1)*hi
	    amou1=ro1*vs1*vs1
	    sinth0=p*vs1
	    costh0=sqrt(1.-sinth0*sinth0)
	    s1=-sin(fi0)
	    s2=cos(fi0)
	    r1=s2*sinth0
	    r2=-s1*sinth0
	    r3=costh0
	    t1=r3*s2
	    t2=-r3*s1
	    t3=-sinth0
c
c  Calculate the arrival time and the Green function response
	    x1=(t2*vectn2+t3*vectn3)*(r1*vectm1+r2*vectm2+r3*vectm3)
     +         +(t1*vectm1+t2*vectm2+t3*vectm3)*(r2*vectn2+r3*vectn3)
c
	    cs=cmplx(1.0,0.0)
	    if(r.ne.0.0)then
              if(zp.lt.0.0)then
c
c  ray passes through focus point when zp < 0.0
                cs=cmplx(0.0,1.0)
                zp=-zp
              endif
	      ss=amou1/vs1*sqrt(ro0*vs(1)*vs(1)*p/(ro1*vs1*zp*r))
	    else
	      ss=amou1/vs1*sqrt(ro0*vs(1)/(ro1*vs1*zp*zp))
	    end if
	    cs=cs*ss
	    ta(j)=ta(j)+t
c
	    if(icomp.eq.1)then
	      x22=s2*vectn2*(r1*vectm1+r2*vectm2+r3*vectm3)
     +           +(s1*vectm1+s2*vectm2)*(r2*vectn2+r3*vectn3)
	      bx=bx*coef1
	      coef1=cx*coef2
	      g(j)=-cs*(x1*bx+x22*coef1)
	    elseif(icomp.eq.2)then
	      x22=s2*vectn2*(r1*vectm1+r2*vectm2+r3*vectm3)
     +           +(s1*vectm1+s2*vectm2)*(r2*vectn2+r3*vectn3)
	      by=by*coef1
	      coef2=cy*coef2
	      g(j)=-cs*(x1*by+x22*coef2)
	    elseif(icomp.eq.3)then
	      bz=bz*coef1
	      g(j)=-(cs*x1)*bz
	    end if
c
130	  continue
	  m1=m2+1
140	continue
c
	return
	end
c
c...................................................................c
c  This program work when there is no discontinuity above the Moho  c
c  I will modify it later.                                          c
c                                          Yuehua Zeng              c
c...................................................................c
c
	subroutine ray1(p,zd,zp,sz,i1,r0,rp,r,m,nc,sqrt11,pn)
	dimension hi(20)
	common/model/wid,d1,d2,dso,theta,fi,nlay,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	real*8 p,pp,pn,sqinv,sqrt1,sqrt0,vsi,vspp0,vspp1,aux,sqrt11
  5	continue
	if(nc.eq.1)then
	  do 10 i=1,nlay-1
	    hi(i)=h(i+1)-h(i)
 10	  continue
c
c  Calculate r and its derivative with respect to slowness p
	  if(m.eq.1.and.p.ge.0.0003)then
	    if(p.gt.pn)p=pn
 20	    pp=p*p
	    r0=0.
	    zp=0.
	    do 30 i=1,nlay-1
	      vsi=vs(i)+as(i)*hi(i)
	      vspp0=vs(i)*vs(i)*pp
	      sqrt0=dsqrt(1.d0-vspp0)
	      vspp1=vsi*vsi*pp
	      if(vspp1.ge.1.0)goto 25
	      sqrt1=dsqrt(1.d0-vspp1)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      dr=aux*p
	      if(r0+dr.ge.r)then
 25	        sqrt1=sqrt0-p*as(i)*(r-r0)
	        vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	        zd=(vsi-vs(i))/as(i)
	        zp=((sqrt1-sqrt0)/(pp*as(i)*sqrt0)-sqrt1*zp)/(p*vsi)
	        zd=zd+h(i)
	        return
	      end if
	      r0=r0+dr
	      zp=zp+aux/(sqrt0*sqrt1)
 30	    continue
	    p=p+0.0003
	    goto 20
	  elseif(m.eq.1.and.p.lt.0.0003)then
	    pp=p*p
	    r0=0.
	    rp=0.
	    do 40 i=1,i1
	      vsi=vs(i)+as(i)*hi(i)
	      vspp0=vs(i)*vs(i)*pp
	      vspp1=vsi*vsi*pp
	      sqrt0=dsqrt(1.d0-vspp0)
	      sqrt1=dsqrt(1.d0-vspp1)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      r0=r0+aux*p
	      rp=rp+aux/(sqrt0*sqrt1)
 40	    continue
c
c  Calculate the ray travel time, geomatric spreading
	  elseif(m.eq.2)then
	    hi(i1)=sz-h(i1)
	    r0=0.
	    zp=0.
	    do 60 i=1,i1
	      vsi=vs(i)+as(i)*hi(i)
	      sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      if(as(i).eq.0.)then
	        r0=r0+hi(i)/(vs(i)*sqrt0)
	      else
	        r0=r0+dlog(vsi*(sqrt0+1.)/(vs(i)*(sqrt1+1.)))/as(i)
	      end if
	      zp=zp+aux/(sqrt0*sqrt1)
 60	    continue
	    sqrt0=dsqrt(1.d0-vs(1)*vs(1)*pp)
	    zp=zp*sqrt1*sqrt0/vs(1)
	  end if
	elseif(nc.eq.2)then
 65	  if(p.gt.pn)p=pn
	  pp=p*p
	  hz=0.0
	  aux=1./p-0.5d-7
	  do 70 i=1,nlay-1
	    if(aux.gt.vs(i).and.aux.le.vs(i+1))then
	      n=i
	      hz=h(i)+(aux-vs(i)+0.5d-7)/as(i)
	      goto 80
	    end if
 70	  continue
	  p=1./(vs(nlay-1)+as(nlay-1)*(h(nlay)-h(nlay-1)))+1.e-6
	  if(m.eq.2)print*,' error in ray tracing.'
	  goto 65
c
c  Calculate r and its derivative with respect to slowness p
 80	  do 90 i=1,n-1
	    hi(i)=h(i+1)-h(i)
 90	  continue
	  hi(n)=hz-h(n)
	  if(m.eq.1)then
	    r0=0.
	    zp=0.
	    do 100 i=1,n-1
	      vsi=vs(i)+as(i)*hi(i)
	      sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      dr=aux*p
	      if(r0+dr.ge.r)then
	        sqrt1=sqrt0-p*as(i)*(r-r0)
	        vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	        zd=(vsi-vs(i))/as(i)
	        zp=((sqrt1-sqrt0)/(pp*as(i)*sqrt0)-sqrt1*zp)/(p*vsi)
	        zd=zd+h(i)
	        return
	      end if
	      r0=r0+dr
	      zp=zp+aux/(sqrt0*sqrt1)
100	    continue
	    sqrt0=dsqrt(1.d0-vs(n)*vs(n)*pp)
	    dr=sqrt0/(as(n)*p)
	    if(r0+dr.ge.r)then
	      sqrt1=sqrt0-p*as(n)*(r-r0)
	      vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	      sqrt11=-sqrt1
	      zd=(vsi-vs(n))/as(n)
	      zp=((sqrt1-sqrt0)/(pp*as(n)*sqrt0)-sqrt1*zp)/(p*vsi)
	      zd=zd+h(n)
	      return
	    end if
	    r0=r0+dr
	    zp=zp-1.0/(sqrt0*as(n)*pp)
c
	    if(r0+dr.ge.r)then
	      sqrt0=p*as(n)*(r-r0)
	      vsi=dsqrt(1.d0-sqrt0*sqrt0)/p
	      sqrt11=sqrt0
	      zd=(vsi-vs(n))/as(n)
	      zp=(sqrt0*zp-1./(pp*as(n)))/(p*vsi)
	      zd=zd+h(n)
	      return
	    end if
	    r0=r0+dr
	    zp=zp-1.0/(sqrt0*as(n)*pp)
c
	    do 110 i=n-1,1,-1
	      vsi=vs(i)+hi(i)*as(i)
	      sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      dr=aux*p
	      if(r0+dr.ge.r)then
	        sqrt0=p*as(i)*(r-r0)+sqrt1
	        vsi=dsqrt(1.d0-sqrt0*sqrt0)/p
	        zd=(vsi-vs(i))/as(i)
	        zp=(zp*sqrt0+(sqrt0-sqrt1)/(pp*as(i)*sqrt1))/(p*vsi)
	        zd=zd+h(i)
	        return
	      end if
	      r0=r0+dr
	      zp=zp+aux/(sqrt0*sqrt1)
110	    continue
	    p=p-0.0003
	    goto 65
c
c  Calculate the ray travel time, geomatric spreading
	  elseif(m.eq.2)then
	    r0=0.
	    zp=0.
	    do 130 i=1,n-1
	      vsi=vs(i)+as(i)*hi(i)
	      sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      if(as(i).eq.0.)then
	        r0=r0+hi(i)/(vs(i)*sqrt0)
	      else
	        r0=r0+dlog(vsi*(sqrt0+1.)/(vs(i)*(sqrt1+1.)))/as(i)
	      end if
	      zp=zp+aux/(sqrt0*sqrt1)
130	    continue
c
	    hi(i1)=sz-h(i1)
	    vsi=vs(i1)+as(i1)*hi(i1)
	    hi(i1)=h(i1+1)-sz
	    do 140 i=i1,n-1
	      sqrt0=dsqrt(1.d0-vsi*vsi*pp)
	      sqrt1=dsqrt(1.d0-vs(i+1)*vs(i+1)*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i+1)+vsi)*sqinv
	      if(as(i).eq.0.)then
	        r0=r0+hi(i)/(vs(i)*sqrt0)
	      else
	        r0=r0+dlog(vs(i+1)*(sqrt0+1.)/(vsi*(sqrt1+1.)))/as(i)
	      end if
	      zp=zp+aux/(sqrt0*sqrt1)
	      vsi=vs(i+1)
140	    continue
	    hi(i1)=sz-h(i1)
	    vsi=vs(i1)+hi(i1)*as(i1)
	    hi(i1)=h(i1+1)-sz
	    if(i1.eq.n)then
	      sqrt0=dsqrt(1.d0-vs(n)*vs(n)*pp)
c	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqrt1=abs(sqrt11)
	      sgn=sign(1.d0,sqrt11)
	      if(sqrt1.eq.0.0)print*,' error in ray tracing.'
	      if(as(i).eq.0.)then
	        r0=r0+sgn*hi(i1)/(vsi*sqrt1)+(hz-h(n))/(vs(n)*sqrt0)
	      else
	    	aux=dlog((sqrt0+1.)/(vs(n)*p))+sgn*dlog((sqrt1+1.)/(vsi*p))
	        r0=r0+aux/as(n)
	      end if
	      zp=(sgn*sqrt0+sqrt1)/(as(n)*pp*sqrt0*sqrt1)-zp
	    else
	      sqrt0=dsqrt(1.d0-vs(n)*vs(n)*pp)
	      sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sgn=1.0
	      if(as(i).eq.0.)then
	        r0=r0+2.0*hi(n)/(vs(n)*sqrt0)
	      else
	        aux=(sqrt0+1.)/(vs(n)*p)
	        r0=r0+2.*dlog(aux)/as(n)
	      end if
	      zp=2.0/(sqrt0*as(n)*pp)-zp
	    end if
c
	    sqrt0=dsqrt(1.d0-vs(1)*vs(1)*pp)
	    zp=zp*sgn*sqrt1*sqrt0/vs(1)
	  end if
	elseif(nc.eq.3)then
	  pp=p*p
c
c  Calculate r and its derivative with respect to slowness p
	  if(m.eq.1)then
	    zd=sz
	    zp=dsqrt(1.d0-vs(1)*vs(1)*pp)/(vs(1)*pp*as(i1))
c
c  Calculate the ray travel time, geomatric spreading
	  elseif(m.eq.2)then
	    do 150 i=1,i1-1
	      hi(i)=h(i+1)-h(i)
150	    continue
	    hi(i1)=sz-h(i1)
	    r0=0.
	    do 160 i=1,i1
	      vsi=vs(i)+as(i)*hi(i)
	      sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	      sqrt1=0.0
	      if(i.ne.i1)sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      if(as(i).eq.0.)then
	        r0=r0+hi(i)/(vs(i)*sqrt0)
	      else
	        r0=r0+dlog(vsi*(sqrt0+1.)/(vs(i)*(sqrt1+1.)))/as(i)
	      end if
160	    continue
	  end if
c
c  find the ray take off character at the source (nc=1,2,3)
	else
	  do 170 i=1,i1-1
	    hi(i)=h(i+1)-h(i)
170	  continue
	  hi(i1)=sz-h(i1)
	  pn=1.0/(vs(i1)+as(i1)*hi(i1))-0.5e-7
	  pp=pn*pn
	  r0=0.
	  do 180 i=1,i1
	    vsi=vs(i)+as(i)*hi(i)
	    sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	    sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	    sqinv=1./(sqrt0+sqrt1)
	    r0=r0+hi(i)*(vs(i)+vsi)*sqinv*pn
180	  continue
	  if(abs(r0-r).lt.0.0001)then
	    nc=3
	  elseif(r0.gt.r)then
	    nc=1
	  else
	    nc=2
	  end if
	  goto 5
	end if
	return
	end
c
c...................................................................c
c  This program work when there is no discontinuity above the Moho  c
c  and is an auxilary programs of ray1.                             c
c                                        by Yuehua Zeng             c
c...................................................................c
c
	subroutine ray0(p,zd,sz,i1,r,m,nc,sqrt11)
	dimension hi(20)
	common/model/wid,d1,d2,dso,theta,fi,nlay,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	double precision p,pp,sqinv,sqrt1,sqrt0,vsi,vspp0,vspp1,aux,
     +	                 sqrt11
  5	continue
	if(nc.eq.1)then
	  do 10 i=1,nlay-1
	    hi(i)=h(i+1)-h(i)
 10	  continue
c
c  Calculate r and its derivative with respect to slowness p
	  if(m.eq.1.and.p.ge.0.0003)then
 20	    pp=p*p
	    r0=0.
	    do 30 i=1,nlay-1
	      vsi=vs(i)+as(i)*hi(i)
	      vspp0=vs(i)*vs(i)*pp
	      sqrt0=dsqrt(1.d0-vspp0)
	      vspp1=vsi*vsi*pp
	      if(vspp1.ge.1.0)goto 25
	      sqrt1=dsqrt(1.d0-vspp1)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      dr=aux*p
	      if(r0+dr.ge.r)then
 25	        sqrt1=sqrt0-p*as(i)*(r-r0)
	        vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	        zd=(vsi-vs(i))/as(i)+h(i)
	        return
	      end if
	      r0=r0+dr
 30	    continue
	    p=p+0.0003
	    goto 20
	  elseif(m.eq.1.and.p.lt.0.0003)then
	    pp=p*p
	    r0=0.
	    do 40 i=1,i1
	      vsi=vs(i)+as(i)*hi(i)
	      vspp0=vs(i)*vs(i)*pp
	      vspp1=vsi*vsi*pp
	      sqrt0=dsqrt(1.d0-vspp0)
	      sqrt1=dsqrt(1.d0-vspp1)
	      sqinv=1./(sqrt0+sqrt1)
	      aux=hi(i)*(vs(i)+vsi)*sqinv
	      r0=r0+aux*p
 40	    continue
	  end if
	elseif(nc.eq.2)then
	  pp=p*p
	  hz=0.0
	  aux=1./p
	  do 70 i=1,nlay-1
	    hz=h(i)
	    if(aux.gt.vs(i).and.aux.le.vs(i+1))then
	      n=i
	      hz=hz+(aux-vs(i))/as(i)
	      goto 80
	    end if
 70	  continue
	  print*,' error in ray tracing.'
	  stop
c
c  Calculate r and its derivative with respect to slowness p
 80	  do 90 i=1,n-1
	    hi(i)=h(i+1)-h(i)
 90	  continue
	  hi(n)=hz-h(n)
	  r0=0.
	  do 100 i=1,n-1
	    vsi=vs(i)+as(i)*hi(i)
	    sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	    sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	    sqinv=1./(sqrt0+sqrt1)
	    aux=hi(i)*(vs(i)+vsi)*sqinv
	    dr=aux*p
	    if(r0+dr.ge.r)then
	      sqrt1=sqrt0-p*as(i)*(r-r0)
	      vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	      zd=(vsi-vs(i))/as(i)+h(i)
	      return
	    end if
	    r0=r0+dr
100	  continue
	  sqrt0=dsqrt(1.d0-vs(n)*vs(n)*pp)
	  dr=sqrt0/(as(n)*p)
	  if(r0+dr.ge.r)then
	    sqrt1=sqrt0-p*as(n)*(r-r0)
	    vsi=dsqrt(1.d0-sqrt1*sqrt1)/p
	    sqrt11=-sqrt1
	    zd=(vsi-vs(n))/as(n)+h(n)
	    return
	  end if
	  r0=r0+dr
c
	  if(r0+dr.ge.r)then
	    sqrt0=p*as(n)*(r-r0)
	    vsi=dsqrt(1.d0-sqrt0*sqrt0)/p
	    sqrt11=sqrt0
	    zd=(vsi-vs(n))/as(n)+h(n)
	    return
	  end if
	  r0=r0+dr
c
	  do 110 i=n-1,1,-1
	    vsi=vs(i)+hi(i)*as(i)
	    sqrt0=dsqrt(1.d0-vs(i)*vs(i)*pp)
	    sqrt1=dsqrt(1.d0-vsi*vsi*pp)
	    sqinv=1./(sqrt0+sqrt1)
	    aux=hi(i)*(vs(i)+vsi)*sqinv
	    dr=aux*p
	    if(r0+dr.ge.r)then
	      sqrt0=p*as(i)*(r-r0)+sqrt1
	      vsi=dsqrt(1.d0-sqrt0*sqrt0)/p
	      zd=(vsi-vs(i))/as(i)+h(i)
	      return
	    end if
	    r0=r0+dr
110	  continue
	  print*,' error in ray tracing.'
	  stop
	end if
	return
	end
c
c........................
c
	subroutine Greens(theta,fi,bx,by,bz,cx,cy)
	common/model/wid,d1,d2,dso,theta1,fi1,n,h(20),vp(20),ap(20),
     +		     vs(20),as(20)
	complex bx,by,bz,cc,cd
	pi=3.1415926
	amou=(1.7+0.2*vp(1))*vs(1)*vs(1)
c
	cosfi=cos(fi)
	sinfi=sin(fi)
c
	sinth=sin(theta)
	costh=cos(theta)
	sinth2=sin(2.*theta)
	costh2=cos(2.*theta)
	cc=cmplx(vs(1)*vs(1)/(vp(1)*vp(1))-sinth*sinth,0.)
	cc=csqrt(cc)
	pr=0.5/(pi*amou)
	cd=costh*pr/(costh2*costh2+2.*sinth2*sinth*cc)
c
	by=costh2*cd
c
	bx=by*cosfi
	by=by*sinfi
	bz=-2.*sinth*cc*cd
c
	cx=-sinfi*pr
	cy=cosfi*pr
c
	return
	end
c
c......................
c
	SUBROUTINE COEFPS(P,VP1,VS1,RO1,VP2,VS2,RO2,NCODE,RMOD,RPH)
C
C     ----------------------------------------------------------------
C
C     THE ROUTINE COEF8 IS DESIGNED FOR THE COMPUTATION OF REFLECTION
C     AND TRANSMISSION COEFFICIENTS AT A PLANE INTERFACE BETWEEN TWO
C     HOMOGENEOUS SOLID HALFSPACES OR AT A FREE SURFACE OF A HOMOGENEOUS
C     SOLID HALFSPACE.
C
C     THE CODES OF INDIVIDUAL COEFFICIENTS ARE SPECIFIED BY THE
C     FOLLOWING NUMBERS
C     A/ INTERFACE BETWEEN TWO SOLID HALFSPACES
C     P1P1...1       P1S1...2       P1P2...3       P1S2...4
C     S1P1...5       S1S1...6       S1P2...7       S1S2...8
C     B/ FREE SURFACE (FOR RO2.LT.0.00001)
C     PP.....1       PX.....5       PX,PZ...X- AND Z- COMPONENTS OF THE
C     PS.....2       PZ.....6       COEF.OF CONVERSION,INCIDENT P WAVE
C     SP.....3       SX.....7       SX,SZ...X- AND Z- COMPONENTS OF THE
C     SS.....4       SZ.....8       COEF.OFCCONVERSION,INCIDENT S WAVE
C
C     I N P U T   P A R A M E T E R S
C           P...RAY PARAMETER
C           VP1,VS1,RO1...PARAMETERS OF THE FIRST HALFSPACE
C           VP2,VS2,RO2...PARAMETERS OF SECOND HALFSPACE. FOR THE FREE
C                    SURFACE TAKE RO2.LT.0.00001,EG.RO2=0., AND
C                    ARBITRARY VALUES OF VP2 AND VS2
C           NCODE...CODE OF THE COMPUTED COEFFICIENT
C
C     O U T P U T   P A R A M E T E R S
C           RMOD,RPH...MODUL AND ARGUMENT OF THE COEFFICIENT
C
C     N O T E S
C     1/ POSITIVE P...IN THE DIRECTION OF PROPAGATION
C     2/ POSITIVE S...TO THE LEFT FROM P
C     3/ TIME FACTOR OF INCIDENT WAVE ... EXP(-I*OMEGA*T)
C     4/ FORMULAE ARE TAKEN FROM CERVENY ,MOLOTKOV, PSENCIK, RAY METHOD
C        IN SEISMOLOGY, PAGES 30-35. DUE TO THE NOTE 2, THE SIGNS AT
C        CERTAIN COEFFICIENTS ARE OPPOSITE
C
C       WRITTEN BY V.CERVENY,1976
C
C     ---------------------------------------------------------------
C
	COMPLEX B(4),RR,C1,C2,C3,C4,H1,H2,H3,H4,H5,H6,H,HB,HC
	DIMENSION PRMT(4),D(4),DD(4)
	double precision p
	IF(RO2.LT.0.000001)GO TO 150
	PRMT(1)=VP1
	PRMT(2)=VS1
	PRMT(3)=VP2
	PRMT(4)=VS2
	A1=VP1*VS1
	A2=VP2*VS2
	A3=VP1*RO1
	A4=VP2*RO2
	A5=VS1*RO1
	A6=VS2*RO2
	Q=2.*(A6*VS2-A5*VS1)
	PP=P*P
	QP=Q*PP
	X=RO2-QP
	Y=RO1+QP
	Z=RO2-RO1-QP
	G1=A1*A2*PP*Z*Z
	G2=A2*X*X
	G3=A1*Y*Y
	G4=A4*A5
	G5=A3*A6
	G6=Q*Q*PP
	DO 21 I=1,4
	  DD(I)=P*PRMT(I)
21	D(I)=SQRT(ABS(1.-DD(I)*DD(I)))
	IF(DD(1).LE.1..AND.DD(2).LE.1..AND.DD(3).LE.1..AND.DD(4).LE.1.)
     1  GO TO 100
C
C     COMPLEX COEFFICIENTS
	DO 22 I=1,4
	  IF(DD(I).GT.1.)GO TO 23
	  B(I)=CMPLX(D(I),0.)
	  GO TO 22
23	  B(I)= CMPLX(0.,D(I))
22	CONTINUE
	C1=B(1)*B(2)
	C2=B(3)*B(4)
	C3=B(1)*B(4)
	C4=B(2)*B(3)
	H1=G1
	H2=G2*C1
	H3=G3*C2
	H4=G4*C3
	H5=G5*C4
	H6=G6*C1*C2
	H=1./(H1+H2+H3+H4+H5+H6)
	HB=2.*H
	HC=HB*P
	GO TO (1,2,3,4,5,6,7,8),NCODE
1	RR=H*(H2+H4+H6-H1-H3-H5)
	GO TO 26
2	RR=VP1*B(1)*HC*(Q*Y*C2+A2*X*Z)
	GO TO 26
3	RR=A3*B(1)*HB*(VS2*B(2)*X+VS1*B(4)*Y)
	GO TO 26
4	RR=-A3*B(1)*HC*(Q*C4-VS1*VP2*Z)
	GO TO 26
5	RR=-VS1*B(2)*HC*(Q*Y*C2+A2*X*Z)
	GO TO 26
6	RR=H*(H2+H5+H6-H1-H3-H4)
	GO TO 26
7	RR=A5*B(2)*HC*(Q*C3-VP1*VS2*Z)
	GO TO 26
8	RR=A5*B(2)*HB*(VP1*B(3)*Y+VP2*B(1)*X)
	GO TO 26
C     REAL COEFFICIENTS
100	E1=D(1)*D(2)
	E2=D(3)*D(4)
	E3=D(1)*D(4)
	E4=D(2)*D(3)
	S1=G1
	S2=G2*E1
	S3=G3*E2
	S4=G4*E3
	S5=G5*E4
	S6=G6*E1*E2
	S=1./(S1+S2+S3+S4+S5+S6)
	SB=2.*S
	SC=SB*P
	GO TO (101,102,103,104,105,106,107,108),NCODE
101	R=S*(S2+S4+S6-S1-S3-S5)
	GO TO 250
102	R=VP1*D(1)*SC*(Q*Y*E2+A2*X*Z)
	GO TO 250
103	R=A3*D(1)*SB*(VS2*D(2)*X+VS1*D(4)*Y)
	GO TO 250
104	R=-A3*D(1)*SC*(Q*E4-VS1*VP2*Z)
	GO TO 250
105	R=-VS1*D(2)*SC*(Q*Y*E2+A2*X*Z)
	GO TO 250
106	R=S*(S2+S5+S6-S1-S3-S4)
	GO TO 250
107	R=A5*D(2)*SC*(Q*E3-VP1*VS2*Z)
	GO TO 250
108	R=A5*D(2)*SB*(VP1*D(3)*Y+VP2*D(1)*X)
	GO TO 250
C
C     EARTHS SURFACE,COMPLEX COEFFICIENTS AND COEFFICIENTS OF CONVERSION
150	A1=VS1*P
	A2=A1*A1
	A3=2.*A2
	A4=2.*A1
	A5=A4+A4
	A6=1.-A3
	A7=2.*A6
	A8=2.*A3*VS1/VP1
	A9=A6*A6
	DD(1)=P*VP1
	DD(2)=P*VS1
	DO 151 I=1,2
151	  D(I)=SQRT(ABS(1.-DD(I)*DD(I)))
	IF(DD(1).LE.1..AND.DD(2).LE.1.)GO TO 200
	DO 154 I=1,2
	  IF(DD(I).GT.1.)GO TO 155
	  B(I)=CMPLX(D(I),0.)
	  GO TO 154
155	  B(I)= CMPLX(0.,D(I))
154	CONTINUE
	H1=B(1)*B(2)
	H2=H1*A8
	H=1./(A9+H2)
	GO TO (161,162,163,164,165,166,167,168),NCODE
161	RR=(-A9+H2)*H
	GO TO 26
162	RR=A5*B(1)*H*A6
	GO TO 26
163	RR=A5*B(2)*H*A6*VS1/VP1
	GO TO 26
164	RR=-(A9-H2)*H
	GO TO 26
165	RR=A5*H1*H
	GO TO 26
166	RR=A7*B(1)*H
	GO TO 26
167	RR=A7*B(2)*H
	GO TO 26
168	RR=-A5*VS1*H1*H/VP1
26	Z2=REAL(RR)
	Z3=AIMAG(RR)
	IF(Z2.EQ.0..AND.Z3.EQ.0.)GO TO 157
	RMOD=SQRT(Z2*Z2+Z3*Z3)
	RPH=ATAN2(Z3,Z2)
	RETURN
157	RMOD=0.
	RPH=0.
	RETURN
C
C     EARTHS SURFACE,REAL COEFFICIENTS AND COEFFICIENTS OF CONVERSION
200	S1=D(1)*D(2)
	S2=A8*S1
	S=1./(A9+S2)
	GO TO (201,202,203,204,205,206,207,208),NCODE
201	R=(-A9+S2)*S
	GO TO 250
202	R=A5*D(1)*S*A6
	GO TO 250
203	R=A5*D(2)*S*A6*VS1/VP1
	GO TO 250
204	 R=(S2-A9)*S
	GO TO 250
205	R=A5*S1*S
	GO TO 250
206	R=A7*D(1)*S
	GO TO 250
207	R=A7*D(2)*S
	GO TO 250
208	R=-A5*VS1*S1*S/VP1
250	IF(R.LT.0.)GO TO 251
	RMOD=R
	RPH=0.
	RETURN
251	RMOD=-R
	RPH=-3.1415926
	RETURN
	END
	SUBROUTINE COEFSH(P,VP1,VS1,RO1,VP2,VS2,RO2,NCODE,RMOD,RPH)
C
C     ----------------------------------------------------------------
C
C     THE CODES OF INDIVIDUAL COEFFICIENTS ARE SPECIFIED BY THE
C     FOLLOWING NUMBERS
C     A/ INTERFACE BETWEEN TWO SOLID HALFSPACES
C     S1S1...1       S1S2...2
C
C     I N P U T   P A R A M E T E R S
C           P...RAY PARAMETER
C           VP1,VS1,RO1...PARAMETERS OF THE FIRST HALFSPACE
C           VP2,VS2,RO2...PARAMETERS OF SECOND HALFSPACE
C           NCODE...CODE OF THE COMPUTED COEFFICIENT
C
C     O U T P U T   P A R A M E T E R S
C           RMOD,RPH...MODUL AND ARGUMENT OF THE COEFFICIENT
C
C     ---------------------------------------------------------------
C
	double precision p
	p1=p*vs1
	p1=sqrt(1.-p1*p1)*vs1*ro1
	p2=p*vs2
	if(abs(p2).ge.1.)goto 1
	p2=sqrt(1.-p2*p2)*vs2*ro2
	if(ncode.eq.1)then
	  rmod=(p1-p2)/(p1+p2)
	  rph=0.
	elseif(ncode.eq.2)then
	  rmod=2.*p1/(p1+p2)
	  rph=0.
	end if
	return
c
c  complex coeficient
1	p2=sqrt(p2*p2-1.)*vs2*ro2
	if(ncode.eq.1)then
	  rmod=1.
	  rph=-2.*atan2(p2,p1)
	elseif(ncode.eq.2)then
	  write(6,'(a)')' The transmitted wave do not exist !'
	  stop
	end if
	return
	end
c
c..............................................
c
      subroutine fork(lx,cx,signi)
      complex cx(lx),carg,cexp,cw,ctemp
      j=1                             
      sc=sqrt(1./float(lx))          
      do i=1,lx                     
        if(i.le.j) then
          ctemp=cx(j)*sc           
          cx(j)=cx(i)*sc          
          cx(i)=ctemp            
        end if
        m=lx/2                  
 20     if(j.le.m)go to 30     
        j=j-m                 
        m=m/2                
        if(m.ge.1)go to 20  
 30     j=j+m              
      end do
      l=1                 
 40   istep=2*l          
      do m=1,l          
        carg=(0.,1.)*(3.14159265*signi*float(m-1))/float(l)
        cw=cexp(carg)  
        do i=m,lx,istep
          ctemp=cw*cx(i+l) 
          cx(i+l)=cx(i)-ctemp
          cx(i)=cx(i)+ctemp 
        end do
      end do
      l=istep
      if(l.lt.lx)go to 40
      return
      end
