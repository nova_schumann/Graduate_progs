import numpy as np

epicenter = [121.081, 22.883]
component_index_1 = 1
component_index_2 = 2
component_index_3 = 3
EARTHQUAKE_TIME = [2,19.54]
PI = 4*np.arctan(1)
D2R = PI/180

class quake_data:
    def __init__(self, station_name, station_lat1, station_lat2, station_log1, 
    station_log2, tsmip_site, tsmip_lat, tsmip_log):
        self.station_name = station_name
        self.station_lat1 = station_lat1
        self.station_lat2 = station_lat2
        self.station_log1 = station_log1
        self.station_log2 = station_log2
        self.tsmip_site = tsmip_site
        self.tsmip_lat = tsmip_lat
        self.tsmip_log = tsmip_log

def calc_distance(latitude, logitude):
    distance_from_epicenter = np.sqrt((epicenter[1]-latitude)**2 + 
        (epicenter[0]-logitude)**2)
    return distance_from_epicenter
    

"""
A function to calculate great circle distances and azimuth between two points on 
the earth'h surface. the earth is assummed to be an ellipsoid of revolution. This 
routine is from a program by lynn peseckis, 1979.

input parameters :
epicenter_latitude, epicenter_logitude...latitude and longitude of first point
    on earth's surface. north latitude and east longitude is positive, south
    latitude and west longitude is negative.
seis_lat, seis_log ......................latitude and longitude of second point 
    on earth's surface.

returned parameters :
    distance_degree ....distance in degrees between two points.
    distance_epi .......distance in kilometers between two points.
    azimuth ............azimuth from first point to second.
    back_azimuth........back azimuth from second point to first.
  
Original created by Yuehua Zeng at USC
"""
def azimuth_distance(epicenter_latitude, epicenter_logitude, seis_lat, seis_log,
    distance_epi):
    ## constants
    DR=57.295780
    EARTH_RADIUS = 6371
    GEOCEN = 0.993305458
    KPERD = 111.18
    ## Calculation for the distance
    distance_logitude = np.absolute(epicenter[0]-seis_log)
    if np.abs(epicenter[1])-90 == 0:
        bp = 90-epicenter[1]
        ap = 90-np.arctan(GEOCEN*np.tan(seis_lat/DR))*DR
        if epicenter[1] <= 0:
            azimuth = 0
            back_azimuth = 180
            distance_degree = np.absolute(ap-bp)
            distance = distance_degree*KPERD
        elif epicenter[1] > 0:
            azimuth = 180
            back_azimuth = 0
    else:
        bp = 90-np.arctan(GEOCEN*np.tan(epicenter[1]/DR))*DR

    if np.absolute(seis_lat)-90==0:
        ap = 90-seis_lat
        if seis_lat < 0:
            azimuth = 180
            back_azimuth = 0
        elif seis_lat >= 0:
            azimuth = 0
            back_azimuth = 180
            distance_degree = np.absolute(ap-bp)
            distance = distance_degree*KPERD
    else:
        ap = 90-np.artan(GEOCEN*np.tan(seis_lat/DR))*DR

    if distance_logitude-0x001 >= 0:
        gamma - (seis_lat-epicenter[0])/DR
        cbp = np.cos(bp/DR)
        sbp = np.sin(bp/DR)
        cap = np.cos(ap/DR)
        sap = np.sin(ap/DR)
        abtem = cbp*cap+sbp*sap*np.cos(gamma)
        c = np.arctan(np.sqrt(1-abtem**2)/abtem)
        if abtem <= 0:
            c = 180/DR + c
            distance_degree = c*DR
            altem = (cap-abtem*cbp)/(np.sin(c)*sbp)
            azimuth = np.arctan(np.sqrt(1-altem**2)/altem)*DR
            if altem < 0:
                azimuth = 180+azimuth
                baztem = (cbp-abtem*cap)/(np.sin(c)*sap)
                back_azimuth = np.arctan(np.sqrt(1-baztem**2)/baztem)*DR
                if baztem < 0:
                    back_azimuth = 180 + back_azimuth
                elif sin(gamma) < 0:
                    azimuth= 360-azimuth
                else:
                    back_azimuth = 360 -  back_azimuth
    azimuth_dist = {distance_degree, distance_epi, azimuth, back_azimuth}
    retrun azimuth_dist

#if __name__ == __main__:
def main_prog():
    with open ("namelist2.txt", "r") as seis_site:
        station_num = seis_site.readline()
        with open("seis.dat", "w") as f_seis:
            f_seis.write(station_num*2)

            ## read the data in "namelist2.txt"
            for i in range(station_num):
                station_record = list(seis_site.readline())
                time_receive = station_record[1]
                time_end = station_record[2]
                plot_scale_1 = station_record[3]
                plot_scale_2 = station_record[4]
                quake_time = station_record[5]
                with open ("acc.cwb", "r") as cwb_all:
                    while line := cwb_all.readline():
                        station_line = list(line)
                        stalist = quake_data()                  
                        #use the class "quake_data"
                        stalist.station_name = station_line[0]
                        stalist.station_lat1 = station_line[1]
                        stalist.station_lat2 = station_line[2]
                        stalist.station_log1 = station_line[3]
                        stalist.station_log2 = station_line[4]
                        '''
                        If the observed data match data in acc.cwb,
                        write data into "stations.txt"
                        '''
                        ew_direction_list = []
                        ns_direction_list = []
                        if stalist.station_name == station_record[0]:
                            with open("stations.txt", "w") as f_stations:
                                stalist.tsmip_site = stalist.station_name
                                stalist.tsmip_lat = (station_lat1+station_lat2)/60
                                stalist.tsmip_log = (station_log1+station_log2)/60
                                f_stations.write(stalist.tsmip_log, stalist.tsmip_lat, 
                                stalist.tsmip_site)
                                '''
                                Calculate distance between epicenter and strong motion
                                site
                                '''
                                distance_epicenter = calc_distance(stalist.tsmip_lat, stalist.tsmip_log)

                                distance_circle = azimuth_distance(epicenter[1], epicenter[0], 
                                stalist.tsmip_lat, stalist.tsmip_log, distance_epicenter)
                                observe_x_direction = distance_epicenter*np.np.cos(D2R*(distance_circle[2] -
                                    fault_strike))
                                observe_y_direction = distance_epicenter*np.np.sin(D2R*(distance_circle[2] -
                                    fault_strike))
                                with open(stalist.tsmip_site, "r") as tsmip_file:
                                    ## Read the 1st 3 lines
                                    for i in range(3):
                                        string = tsmip_file.readline()
                                        time = list(tsmip_file.readline())
                                        number_samplerate = list(tsmip_file.readline())
                                        number = number_samplerate[0]
                                        sample_rate = number_samplerate[1]
                                        time_difference = time_receive - time_end
                                        data_count = 0
                                        print(time_difference)
                                        for j in range(number):
                                            swave_record = list(tsmip_file.readline())
                                            swave_time = swave_record[0]
                                            swave_ew = swave_record[1]
                                            swave_ns = swave_record[2]
                                            ## Store the whole S-wave records
                                            swave_group=[]
                                            swave_param_group_ew = []
                                            swave_param_group_ns = []
                                            swave_record_group_ew = []
                                            swave_record_group_ns = []
                                            
                                            ## Store s-wave record every time we read from the file
                                            swave_mod = []
                                            swave_record_count = 0
                                            # picking the S-wave time
                                            if swave_time >= time_receive and swave_time <= time_end:
                                                swave_record_count += 1
                                                swave_mod = [swave_time, swave1, swave2]
                                                swave_group.append(swave_mod)
                                                dif1 = dif2 = 0
                                                ## reduce the sample number
                                                sample_rate_new = sample_rate*32
                                                data_count = swave_record_count/32
                                                #ew_swave = [(swave_time*32+1) - time_difference - dif1, swave_ew*32+1]
                                    ## Put necessary values into the list -- East-West
                                    ew_direction_list = [observe_x_direction,observe_y_direction,
                                     data_count, component_index_1, sample_rate_new, plot_scale_1, 
                                     stalist.tsmip_site]
                                    ew_direct_simple = [data_count, stalist.tsmip_site]
                                    f_seis.write(ew_direction_list)
                                    swave_param_group_ew.append(ew_direction_list)

                                    ### Write down S-wave recoed with reduced data
                                    swave_record_ew = [[swave_group[0::32][i][0]-time_difference-dif1,
                                     swave_group[0::32][i][1]] for i in range(len(swave_group))]
                                    f_seis.write(swave_record_ew)
                                    swave_record_group_ew.append(swave_record_ew)
                                    

                                    ## Put necessary values into the list -- North-South
                                    ns_direction_list = [observe_x_direction,observe_y_direction,
                                     data_count, component_index_2, sample_rate_new, plot_scale_2, 
                                     stalist.tsmip_site]
                                    ns_direct_simple = [data_count, stalist.tsmip_site]
                                    f_seis.write(ns_direction_list)
                                    swave_param_group_ns.append(ns_direction_list)

                                    swave_record_ns = [[swave_group[0::32][i][0]-time_difference-dif1,
                                     swave_group[0::32][i][2]] for i in range(len(swave_group))]
                                    f_seis.write(swave_record_ns)
                                    swave_record_group_ns.append(swave_record_ns)

                                    with open("seis3-1.dat", "w")  as f_seis31:
                                        f_seis31.write(station_num*2)
                                        f_seis31.write(ew_direction_list)
                                        f_seis31.write(ew_direct_simple)
                                        f_seis31.write(swave_record_ew)

                                        f_seis31.write(ns_direction_list)
                                        f_seis31.write(ns_direct_simple)
                                        f_seis31.write(swave_record_ns)
                            f_stations.close()
                cwb_all.close()
        f_seis.close()
        f_seis31.close()
    seis_site.close()
    return  ew_param_group_ew, swave_record_group_ew, ns_param_group_ns, swave_record_group_ns