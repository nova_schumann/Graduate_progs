
	character stn(120)*12,name(120)*12
	real*8 xobs(120),yobs(120),tr(120),td(120),anorm,sampr
	real*8 t(20000),a1(20000),a2(20000),a3(20000),tt(20000),aa1(20000),aa2(20000),aa3(20000)
	real dif,ddif,diff,anorm1(120),anorm2(120),dif1,dif2
	
	real pi,d2r,epla,eplo,slat(50),slon(50),azm,dist
	real str1,slat2(725),slon2(725),dis,t0(50)
	integer no1,slat1(725),slon1(725)
	character sname(725)*6
	character site*7,string*6
	
	integer icomp1,icomp2,m,m1,no,e_min,e_sec,s_min,s_sec
	
	icomp1=1
	icomp2=2
	icomp3=3

    !starting time of quake
	e_min=2
	e_sec=19.54
	pi=4.*atan(1.)
	d2r=pi/180.

    epla=22.883			!latitude

    eplo=121.081		!logitude
	print*,'input strike-angle'
	read *,str1
	

	open(10,file='namelist2.txt')
	open(9,file='stations.txt')
	open(11,file='acc.cwb')
	open(31,file='seis3-1.dat')
	do i=1,725
	read(11,*) sname(i),slat1(i),slat2(i),slon1(i),slon2(i)
	    slat2(i)=slat1(i)+slat2(i)/60.
	    slon2(i)=slon1(i)+slon2(i)/60.
	end do 

	read(10,*) no1
	do i=1,no1
	  read(10,*) stn(i),tr(i),td(i),anorm1(i),anorm2(i),t0(i)
	  do j=1,725
	    if(stn(i)==sname(j)) then
          slat(i)=slat2(j)
	      slon(i)=slon2(j)
	      write(9,*) slon(i),slat(i),'  ',stn(i)					!output file "stations.txt"
	    end if
	  end do
	  dis=sqrt((epla-slat(i))**2+(eplo-slon(i))**2)

	  call distaz(epla,eplo,slat(i),slon(i),dist,azm)				!Call the sub-program 'distaz'
	  xobs(i)=dist*cos(d2r*(azm-str1))
	  yobs(i)=dist*sin(d2r*(azm-str1))
	  name(i)=stn(i)
	end do 
	
	open(30,file='seis.dat')
	write(30,'(i5)') no1*2								    !output number of sites
	do i=1,no1
	open(12,file=name(i))

	!start reading first 3 lines
		read (12,*) string
		read (12,"(14x,i2,1x,i2)") s_min,s_sec			
		read (12,*) no,sampr
	
	!calculate the difference of start time between instrument and quake
!			diff=((e_min*60.+e_sec)-(s_min*60.+s_sec))			
        diff=tr(i)-t0(i)
		print*,diff
	!start  reading recorded data
			m=0
	do j=1,no
	  read(12,*) t(j),a2(j),a3(j)
	  if(t(j)>=tr(i).and.t(j)<=td(i)) then		!start picking data of S-wave time
	    m=m+1
	    tt(m)=t(j)
	    aa2(m)=a2(j)
		aa3(m)=a3(j)
      end if    

	end do 

	sampr=sampr*32					!because earlier memory isn't enough-->reduse samples
	m1=m/32
	
	if(m1>=230) m1=230 
	!if(name(i)=='TTN002') dif=4.9414
	!if(name(i)=='TTN003') dif2=-0.6
	!if(name(i)=='TTN012') dif=2.1500
	!if(name(i)=='TTN013') dif=0
	!if(name(i)=='TTN014') dif=7.8073
	!if(name(i)=='TTN015') dif=2.4089
	!if(name(i)=='TTN016') dif=10.9668
	!if(name(i)=='TTN017') dif=16.4731
	!if(name(i)=='TTN020') dif=0
	!if(name(i)=='TTN021') dif=6.3435
	!if(name(i)=='TTN022') dif=6.4065
	!if(name(i)=='TTN024') dif=2.6102
	!if(name(i)=='TTN025') dif=1.5993
	if(name(i)=='TTN027') dif1=0.3
	if(name(i)=='TTN027') dif2=-0.3
	if(name(i)=='TTN029') dif1=-0.7
	if(name(i)=='TTN029') dif2=-1.0
	!if(name(i)=='TTN031') dif=12.7445
	!if(name(i)=='TTN032') dif=10.5366
	if(name(i)=='TTN034') dif1=0.5
	!if(name(i)=='TTN035') dif=3.4972
	if(name(i)=='TTN036') dif1=0.30
	!if(name(i)=='TTN037') dif=8.7085
	!if(name(i)=='TTN038') dif=8.8610
	!if(name(i)=='TTN041') dif=6.9052
	!if(name(i)=='TTN042') dif1=-0.3
	!if(name(i)=='TTN042') dif2=0
	!if(name(i)=='TTN043') dif=6.0784
	!if(name(i)=='TTN044') dif=3.6109
	if(name(i)=='TTN045') dif2=-0.3
	!if(name(i)=='TTN046') dif=4.2663
	!if(name(i)=='TTN048') dif=1.9832
	if(name(i)=='TTN049') dif2=0.2
	!if(name(i)=='TTN050') dif=5.1960
	!if(name(i)=='TTN051') dif=8.1977
	!if(name(i)=='TTN052') dif=6.4726
	!if(name(i)=='TTN053') dif=10.9087
	!if(name(i)=='TTN054') dif=2.1514
	!ddif=tt(1)-dif  
    !dif1=0
	!dif2=0

	!write down E-W direction
	write(31,'(i3,1x,a6)')m1,name(i)	                
	write(31,'(e12.6,2x,e12.6)') (tt((j-1)*32+1)-diff-dif1,aa2((j-1)*32+1),j=1,m1)
	write(30,'(2f10.5,2i5,2f10.5,a6)')xobs(i),yobs(i),m1,icomp1,sampr,anorm1(i),name(i)
    write(30,'(10e12.6)') (tt((j-1)*32+1)-diff-dif1,aa2((j-1)*32+1),j=1,m1)

	!write down N-S direction
	write(31,'(i3,1x,a6)')m1,name(i)	                
	write(31,'(e12.6,2x,e12.6)') (tt((j-1)*32+1)-diff-dif2,aa3((j-1)*32+1),j=1,m1)
    write(30,'(2f10.5,2i5,2f10.5,a6)')xobs(i),yobs(i),m1,icomp2,sampr,anorm2(i),name(i)
    write(30,'(10e12.6)') (tt((j-1)*32+1)-diff-dif2,aa3((j-1)*32+1),j=1,m1)  

	close(12)	
	end do
	write (*,*) s_min,s_sec,no1,string
	close(9)
	close(10)
	close(11)
	close(30)
	end
	
	  
      subroutine distaz(epla,eplo,sla,slo,dist,azi)
!
!     subroutine distaz(epla,eplo,sla,slo,del,dist,azi,baz)
!     a subroutine to calculate great circle distances and
!     azimuth between two points on the earth'h surface. the
!     earth is assummed to be an ellipsoid of revolution.
!     this routine is from a program by lynn peseckis, 1979.
!
!     input parameters :
!
!     epla, eplo ........latitude and longitude of first point
!                        on earth's surface. north latitude
!                        and east longitude is positive, south
!                        latitude and west longitude is negative.
!     sla, slo ..........latitude and longitude of second point
!                        on earth's surface.
!
!     returned parameters :
!
!     del ...............distance in degrees between two points.
!     dist ..............distance in kilometers between two points.
!     az ................azimuth from first point to second.
!     baz ...............back azimuth from second point to first.
!
!                                  Compiled by Yuehua Zeng at USC
!
      real*8 c,bp,ap,gamma,cbp,sbp,cap,sap,abtem,altem,baztem
!      implicit integer*2 (i-n)
!
!  basic constants :
      dr=57.295780
      re=6371.0
      geocen = 0.993305458
      kperd = 111.18
!  begin calculation of distance
      dlon=abs(eplo-slo)
      if(abs(epla)-90.0==0.0)then
        bp=90.0-epla
        ap=90.0-(atan(geocen*tan(sla/dr)))*dr
        if(epla) 170,170,180
      else
        bp=90.0-(atan(geocen*tan(epla/dr)))*dr
      endif
      if(abs(sla)-90.0==0.0)then
        ap=90.0-sla
        if(sla) 180,170,170
      else
        ap=90.0-(atan(geocen*tan(sla/dr)))*dr
      endif
      if(dlon-0.001>=0.0)goto 200
      if(epla-sla)170,170,180
170   azi=0.0
      baz=180.0
      goto 190
180   azi=180.0
      baz=0.0
190   del=abs(ap-bp)
      goto 210
200   gamma=(slo-eplo)/dr
      cbp=cos(bp/dr)
      sbp=sin(bp/dr)
      cap=cos(ap/dr)
      sap=sin(ap/dr)
      abtem=cbp*cap+sbp*sap*cos(gamma)
      c=atan(sqrt(1.0-abtem*abtem)/abtem)
      if(abtem<0.0)c=180.0/dr+c
      del=c*dr
      altem=(cap-abtem*cbp)/(sin(c)*sbp)
      azi=atan(sqrt(1.0-altem*altem)/altem)*dr
      if(altem<0.0)azi=180.0+azi
      baztem=(cbp-abtem*cap)/(sin(c)*sap)
      baz=atan(sqrt(1.0-baztem*baztem)/baztem)*dr
      if(baztem<0.0)baz=180.+baz
      if(sin(gamma).lt.0.0)then
        azi=360.-azi
      else
        baz=360.-baz
      endif
210   dist = del*kperd

      return
      end
	   