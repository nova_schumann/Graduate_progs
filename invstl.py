import numpy as numpy
import math

import in_1


'''
......................................................................
             MAIN PROGRAM FOR STRONG MOTION SOURCE INVERSE

    program : invstl.for
    purpose : inverse for the slip intensity distribution from the 
              strong motion seismograms.

    input:
        (1)   num_iterate - number of iterations (not used in this program)
              damp        - damping parameter
              rns         - noise to signal ratio
              dcor_width  - correlation distance in width
              dcor_dip    - correlation distance in dip
			  wavelet_pts - total number of points of the wavelet
              rise_time   - rise time
              low_freq_corner - low frequency corner of the bandpass
              hi_freq_corner  - high frequency corner of the bandpass
              low_freq_cutoff - low frequency transition cut off
              hi_freq_cutoff  - high frequency transition cut off
              fault_width     - width of the fault (this could be a misdefinition)
              fault_upper_dip  - upper fault position along dip (depth/sin(dip))
              fault_lower_dip  - lower fault position along dip (depth/sin(dip))
              hypocenter_width - hypocenter location along width
			  hypocenter_dip   - hypocentral position along dip (depth/cos(dip))
              theta            - fault dip
              phi              - fault rake (in rad)
              num_layer        - number of layer including the half space
              thickness_layer  - layer thickness
              vp - P wave velocity
              ap - P wave velocity gradience
              vs - S wave velocity 
              as - S wave velocity gradience
              gridnum_width, gridnum_length - number of grid lines along width/length

        (2)   rupture_time - rupture time distribution on the fault
                            (numgrid_width*numgrid_length)
              s - slip intensity distribution on the fault dip component
                    (numgrid_width*numgrid_length)
                  
              s - slip intensity distribution on the fault strike component
                    (numgrid_width*numgrid_length)
                  
        (3)   station_x_strike - station coordinate along strike from epicenter
              station_y_strike - station coordinate perpendicular to strike from
                     epicenter
              num_timeseries - total number of time series points
              index_comp     - component index : 1 - along strike 
                                                 2 - perpendicular to strike
                                                 3 - vertical
              plotting_scale - plotting scale constance (not useful here)
              time_step - time step
              site_name - station abbreviation
              t - time series
              obs_seis - observation seismogram
              
    output:
              rupture_time - rupture time distribution on the fault
                            (numgrid_width*numgrid_length)
              s - slip intensity distribution on the fault dip component
                    (numgrid_width*numgrid_length)
              s - slip intensity distribution on the fault strike component
                    (numgrid_width*numgrid_length)
              
                                   Yuehua Zeng, June 6, 1993
......................................................................
'''
# Some constants
PI = math.pi
err = 1e-6

'''
Block diagram of the script:

Main <--|-- covariance
        |-- wavelet ----- fork
        |-- sythetic -|-- derivative --- xdt
                      |-- contour
                      |-- ray_tracing -- coefficient_fps
                                      |- coefficient_fsh
                                      |- greens_function
                                      |- ray_path1
                                      |- ray_path2
'''


def covariance(ar, mmxy, displacement, area_xy2, numgrid_width, numgrid_length,
                 dcor_width, dcor_length):
    # Create the covariance matrix for the model
    area_xy = gridnum_width*gridnum_length
    rev_dcor_width = 1/dcor_width
    rev_dcor_length= 1/dcor_dip

    covariance_ij = 0
    covariance_i = 0
    covariance_j = 0
    for i in range(1, numgrid_width):
        for j in range(1, numgrid_length):
            covariance_i += 1
            for k in range(1, i-1):
                for l in range(numgrid_length):
                    covariance_ij += 1
                    covariance_xi = (k-i)*dcor_width
                    covariance_yj = (l-j)*dcor_length
                    covariance_j += 1
                    ar[covariance_ij]=displacement[covariance_i]*displacement[covariance_j]
                        *np.exp(-(covariance_xi**2 + covariance_yj**2))
            k = i
            for l in range(1, j-1):
                covariance_ij += 1
                covariance_xi = (k-i)*dcor_width
                covariance_yj = (l-j)*dcor_length
                covariance_j += 1
                ar[covariance_ij] = displacement[covariance_i]*displacement[covariance_j]
                        *np.exp(-(covariance_xi**2 + covariance_yj**2))
            covariance_ij += 1
            ar(covariance_ij) = displacement[covariance_i]*displacement[covariance_j]
    for i in range(1, numgrid_width):
        for j in range(1, numgrid_length):
            for k in range(1, area_xy):
                covariance_ij += 1
                ar(covariance_ij) = 0
            covariance_i += 1
            covariance_j = area_xy
            for k in range(1, i-1):
                for l in range(1, numgrid_length):
                    covariance_ij += 1
                    covariance_xi = (k-i)*dcor_width
                    covariance_yj = (l-j)*dcor_length
                    covariance_j += 1
                    ar[covariance_ij] = displacement[covariance_i]*
                        displacement[covariance_j]*np.exp(-(covariance_xi**2+covariance_yj**2))
            k = i
            for l in range(1, j-1):
                covariance_ij += 1
                covariance_xi = (k-i)*dcor_width
                covariance_yj = (l-j)*dcor_length
                covariance_j += 1
                ar[covariance_ij] = displacement[covariance_i]*displacement[covariance_j]
                    *np.exp(-(covariance_xi**2+covariance_yj**2))
            covariance_ij += 1
            ar(covariance_ij) = displacement[covariance_i]*displacement[covariance_j]
    covariance = [ar, mmxy, displacement, area_xy2, numgrid_width, numgrid_length,
                 dcor_width, dcor_length]
    return covariance

def fork(lx, cx, signal_i):
    ## a function for calculating wavelet funciton

    j = 1
    sc = np.sqrt(1/lx)
    for i in range(1, lx):
        if i <= j:
            ctemp = cx[j]*sc
            cx[j] = cx[i]*sc
            cs[i] = ctemp
        m = lx/2
        j = j-m
        m = m/2
        if m >= 1:
            if j <= m:
                j += m
        l = 1
        i_step = 2*l
        for m in range(1, l)
            carg = complex(0,1)*(PI*signal_i*(m-1))/l
            ##cw = complex(carg)
            for i in range(m, lx, i_step):
                ctemp = carg*cx[i + l]
                cx[i+l] = c[i] -ctemp
                cx[i] = c[i] + ctemp
        
        l = i_step
        if l > lx:
            i_step = 2*l

        return [lx, cx, signal_i]

def wavelet(num_wavelet, diff_time, rise_time, low_freq_corner, hi_freq_corner,
     low_freq_cutoff, hi_freq_cutoff ):
    '''
    input: 
        num_wavelet     - total number of points of the wavelet
        diff_time       - time interval
        rise_time       - rise time
        low_freq_corner - low frequency corner of the bandpass
        hi_freq_corner  - high frequency corner of the bandpass
        low_freq_cutoff - low frequency transition cut off
        hi_freq_cutoff  - high frequency transition cut off
    output:
        wavelet         - bandpassed wavelet
        wavelet_hilbert - Hilbert transform of wlet
    '''
    half_pi = PI/2
    cz = complex(0,0)
    ndwi = int(rise_time/diff_time)
    t = 0
    for i in range(1,num_wavelet):
        if i > ndwi+1 :
            wavelet[i] = 0
        if t > diff_time:
            wavelet = np.sqrt(t+diff_time)-np.sqrt(t -diff_time)
        else:
            wavelet = np.sqrt(t+diff_time)
        t += diff_time
    ## extending the length of the seismogram to the power of 2
    if(num_wavelet < 512):
        m = 512
    if(num_wavelet < 1024):
        m = 1024
        
    ## Calculation of dw and Nyquist frequency
    dw = 1/(m*diff_time)
    m_low_corner = int(low_freq_corner/dw)
    m_low_cutoff = int(low_freq_cutoff/dw)
    m_high_corner= int(high_freq_corner/dw)
    m_high_cutoff= int(high_freq_cutoff/dw)
    freq_nyquist = 1/(2*rise_time)
    m2 = m/2 + 1

    ## Calculate FFT of the seismogram
    for i in range(1, num_wavelet):
        v[i] = complex(wavelet[i], 0)
    for i in range(num_wavelet+1, m):
        v[i] = cz
    fork_value = fork(m, v, -1)
    
    for i in range(1, m_low_corner):
        v[i] = cz
    band = -1
    db = 2/(m_low_cutoff - m_low_corner)
    for i in range(m_low_corner+1, m_low_cutoff):
        band += db
        v[i] = v[i]*(0.5 + 0.5*np.sin(half_pi*band))
    band = -1
    db = 2/(m_high_cutoff - m_high_corner)
    for i in range(m_high_corner+1, m_high_cutoff):
        band += db
        v[i] = v[i]*(0.5 - 0.5*np.sin(half_pi*band))
    for i in range(m_high_cutoff+1, m2):
        v[i] = cz
    k = 0
    for i in range(m_high_cutoff+1, m2):
        k += 2
        v[i] = complex(v[i-k].real, -1*v[i-k].imag)
    fork_value2 = fork(m, v, 1)

    ## obtain the time domain wavelet in non-casual form
    for i in range(-1*num_wavelet, -1):
        wavelet[i] = v[m+i+1].real
    for i in range(n):
        wavelet[i] = v[i+1].real
    
    ## Compute the Hilbert transform of the wavelet
    hilbert[0] = 0
    hilbert[1] = 2*np.log(2)/PI
    hilbert[-1] = -1* hilbert[1]
    for i in range(2, 400):
        dd = i
        hilbert[i] = -((1+dd)*log(1+1/dd)+(1-dd)*log(1-1/(1-dd)))/PI
        hilbert[-i] = -1*hilbert[i]

        for i in range(-n, n):
            for j in range(-n, n):
                wavelet_hilbert[i] = wavelet_hilbert[i] + wavelet[j] * hilbert[j-i]
    retrun (num_wavelet, diff_time, rise_time, low_freq_corner, hi_freq_corner,
     low_freq_cutoff, hi_freq_cutoff)
     
def ray_path1(p,zd,zp,sz,i1,r0,rp,radius,m,nc,sqrt11,pn):
    '''
    ...................................................................
    This program work when there is no discontinuity above the Moho 
    I will modify it later.                                         
                                              Yuehua Zeng             
    ...................................................................
    '''
    if nc == 1 :
        for i in range(1, num_layer):
            hi[i] = h[i+1]-h[i]
    
    ## Calculate radius and its derivative with respect to slowness p
    if m == 1 and p >= 0.0003:
        double_p = p*p
        r0 = 0
        for i in range(1, num_layer-1):
            vsi = vs[i] + as[i]*hi[i]
            vspp0 = (vs[i]**2)*double_p
            sqrt0 = np.sqrt(1-vspp0)
            vspp1 = vsi*vsi*double_p
            if vspp1 >= 1 :
                sqrt1 = sqrt0 - p*as[i]*(radius-r0)
                vsi = np.sqrt(1-sqrt1**2)/p
                zd = (vsi-vs[i])/as[i] + h(i)
            sqrt1 = np.squr(1 - vspp1)
            sqiinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            dr = aux*p
            if r0+dr >= radius:
                sqrt1 = sqrt0 - p*as[i]*(radius-r0)
                vsi = np.sqrt(1-sqrt1**2)/p
                zd = (vsi-vs[i])/as[i] + h(i)
            r0 += dr
        p += 0.0003
    elif m == 1 and p < 0.0003:
        double_p = p**2
        r0 = 0
        for i in range(1, i1):
            vsi = vs[i] + as[i]*hi[i]
            vspp0 = (vs[i]**2)*double_p
            sqrt0 = np.sqrt(1-vspp0)
            sqrt1 = np.squr(1-vspp1)
            sqiinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            r0 = r0 + aux*p
    ### Calculate ray travel time, geometric spreading
    elif m == 2:
        hi[i1] = sz - h[i1]
        r0 = 0
        zp = 0
        for i in range(1, i1):
            vsi = vs[i] + as[i]*hi[i]
            vspp0 = (vs[i]**2)*double_p
            sqrt0 = np.sqrt(1-vspp0)
            sqrt1 = np.squr(1-vspp1)
            sqiinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            if as[i] == 0:
                r0 = r0 + hi[i]/(vs[i]*sqrt0)
            else:
                r0 = r0 + np.log(vsi*(sqrt0+1)/(vs[i]*(sqrt0+1)))/as[i]
            sqrt0 = np.sqrt(1-(vs[1]**2)*double_p)
            zp = zp*sqrt0/vs[1]
    elif nc == 2:
        if p > pn :
            p = pn
        double_p = p**2
        hz = 0
        aux = 1/p - 0.5*d - 7
        for i in range(1, num_layer-1):
            if aux > vs[i] and aux <= vs[i+1]:
                n = i
                hz = h[i] + (aux - vs[i] + 0.5*d - 7)/as[i]
                for i in range(1, n-1):
                    hi[i] = h[i+1] - h[i]
        p = 1/(vs[num_layer-1] + as[num_layer-1]*(h[num_layer] -
            h[num_layer-1])) + 1e-6
        if m == 2 :
            print("Error in ray tracing...")

    ## Calculate radius and its derivative with respect to slowness p
    for i in range(1, n-1):
        hi[i] = h[i+1] - h[i]
    hi[n] = hz - h[n]
    if m == 1:
        r0 = 0
        zp = 0
        for i in range(1, n-1):
            vsi = vs[i] + as[i]*h[i]
            sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
            sqrt1 = np.sqrt(1-(vsi**2)*double_p)
            sqinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            dr = aux*p
            if r0+dr >= r:
                sqrt1 = sqrt0 - p*as[i]*(r-0)
                vsi = np.sqrt(1-sqrt1**2)/p
                zd = (vsi-vs[i])/as[i]
                zp = ((sqrt1-sqrt0)/(double_p*as[i]*sqrt0)-sqrt1*zp)/(p*vsi)
                zd += h[i]
            r0 += dr
            zp += aux/(sqrt0*sqrt1)
        sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
        dr = sqrt0/(as[n]*p)
        if r0+dr >= r:
            sqrt1 = sqrt0 - p*as[i]*(r-0)
            vsi = np.sqrt(1-sqrt1**2)/p
            sqrt11 = -sqrt1
            zd = (vsi-vs[i])/as[i]
            zp = ((sqrt1-sqrt0)/(double_p*as[i]*sqrt0)-sqrt1*zp)/(p*vsi)
            zd += h[n]
        r0 += dr
        zp = zp - 1/(sqrt0*as[n]*double_p)

        if r0+dr >= r:
            sqrt1 = sqrt0 - p*as[i]*(r-0)
            vsi = np.sqrt(1-sqrt1**2)/p
            sqrt11 = sqrt0
            zd = (vsi-vs[i])/as[i]
            zp = (sqrt0*zp - double_p*as[i])/(p*vsi)
            zd += h[n]
        r0 += dr
        zp = zp - 1/(sqrt0*as[n]*double_p)

        for i in range(n-1, 1, -1):
            vsi = vs[i] + as[i]*h[i]
            sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
            sqrt1 = np.sqrt(1-(vsi**2)*double_p)
            sqinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            dr = aux*p
            if r0+dr >= r:
                sqrt0 = p*as[i]*(radius-r0) + sqrt1
                vsi = np.sqrt(1-sqrt1**2)/p
                zd = (vsi-vs[i])/as[i]
                zp = ((zp*sqrt0 + (sqrt0-sqrt1))/(double_p*as[i]*sqrt1))/(p*vsi)
                zd += h[i]
            r0 += dr
            zp = zp + aux/(sqrt0+sqrt1)
        p -= 0.003
    ## Calculate the ray travel time, geometric spreading
    elif m == 2:
        r0 = 0
        zp = 0
        for i in range(1, n-1):
            vsi = vs[i] + as[i]*h[i]
            sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
            sqrt1 = np.sqrt(1-(vsi**2)*double_p)
            sqinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]+vsi)*sqinv
            if as[i] == 0:
                r0 += hi[i]/(vs[i]*sqrt0)
            else:
                r0 += np.log(vsi*(sqrt0+1)/(vsi*(sqrt1+1)))/as[i]
            zp += aux/(sqrt0+sqrt1)

        hi[i1] = sz - h[i1]
        vsi += as[i1]*hi[i1]
        hi[i1] = h[i1+1] - sz
        for i in range(i1, n-1):
            sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
            sqrt1 = np.sqrt(1-(vsi**2)*double_p)
            sqrt1 = np.absolute(sqrt11)
            sgn = float(np.copysign(1, sqrt11))
            if sqrt1 == 0:
                print("Error in ray tracing...")
                if as[i] == 0:
                    r0 = r0 + sgn*hi[i1]/(vsi*sqrt1) + (hs-h[n])/(vs[n]*sqrt0)
                else:
                    aux = np.log(sqrt0+1)/(vs[n]*p) + sgn*np.log((sqrt+1)/(vsi*p))
                    r0 += aux/as[n]
                zp = (sgn*sqrt0+sqrt1)/(as[n]*double_p*sqrt0*sqrt1) - zp
            else:
                sqrt0 = np.sqrt(1-(vs[1]**2)*double_p)
                sqrt1 = np.sqrt(1-(vsi**2)*double_p)
                sgn = 1
                if as[i] == 0:
                    r0 += 2*hi[n]/(vs[n]*sqrt0)
                else:
                    aux = (sqrt0+1)/(vs[n]*p)
                    r0 += 2*np.log(aux)/as[n]
                zp = 2/(sqrt0*as[n]*double_p) - zp

            sqrt0 = np.sqrt(1-(vs[1]**2)*double_p)
            zp = zp*sgn*sqrt0*sqrt1/vs[1]
        elif nc == 3:
            double_p = p**2

        ### Calculate radius and its derivative with  slowness p
            if m == 1:
                zd = sz
                zp = np.sqrt(1-(vs[1]**2)*double_p)/(vs[1]*double_p*as[i1])

            #### Calculate ray travel time, geometric spreading
            elif m == 2:
                for i in range(1, i1-1):
                    hi[i] = h[i+1] - h[i]
                hi[i1] = sz - h[i1]
                r0 = 0
                for i in range(1, i1):
                    vsi = vs[i] + as[i]*hi[i]
                    sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
                    sqrt1 = 0
                    if i != i1:
                        sqrt1 = np.sqrt(1-(vsi**2)*double_p)
                        sqinv = 1/(sqrt0+sqrt1)
                        aux = hi[i]*(vs[i]+vsi)*sqinv
                        if as[i] == 0:
                            r0 += hi[i]/(vs[i]*sqrt0)
                        else:
                            r0 += np.log(vsi*(sqrt0+1)/(vs[i]*(sqrt1+1)))/as[i]
                    
                    #### Find the ray take off character at the source (nc=1,2,3)
            else:
                for i in range(1, i1-1):
                    hi[i1] =  h[i+1] - h[i]
                hi[i1] = sz - h[i1]
                pn = 1/(vs[i1]+as[i1]*hi[i1]) - 0.5*e-7
                double_p = pn**2
                r0 = 0
                for i in range(i1):
                    vsi = vs[i] + as[i]*hi[i]
                    sqrt0 = np.sqrt(1-(vs[i]**2)*double_p)
                    sqrt1 = np.sqrt(1-(vsi**2)*double_p)
                    sqinv = 1/(sqrt0+sqrt1)
                    r0 += hi[i]*(vs[i]+vsi)*sqinv*pn
                if np.absolute(r0-radius) < 0.0001:
                    nc = 3
                elif r0 > radius:
                    nc = 1
                else:
                    nc = 2
    return p,zd,zp,sz,i1,r0,rp,radius,m,nc,sqrt11,pn

def ray_path2(p,zd,sz,i1,r,m,nc,sqrt11):
    '''
    ...................................................................
      This program work when there is no discontinuity above the Moho  
      and is an auxilary programs of ray1.                             
                                            by Yuehua Zeng             
    ...................................................................
    '''
    if nc == 1:
        for i in range(num_layer-1):
            hi[i] = h[i+1] - h[i]

        ## Calculate radius and its derivative with slowness p
        while m == 1 and p >= 0.0003:
            double_p = p**2
            r0 = 0
            for i in range(num_layer-1):
                vsi = vs[i] + as[i]*hi[i]
                vspp0 = vs[i]**2*double_p
                sqrt0 = np.sqrt(1-vspp0)
                vspp1 = vsi**2*double_p
                if vspp1 >= 1:
                    sqrt1 = sqrt0 - p*as[i]*(radius-r0)
                    vsi = np.sqrt(1-sqrt1**2)/p
                    zd = (vsi-vs[i])/as[i] + h[i]
                sqrt1 = np.sqrt(1-vspp1)
                sqinv = 1/(sqrt0+sqrt1)
                aux = hi[i]*(vs[i]*vsi)*sqinv
                dr = aux*p
                if (ro+dr) >= radius:
                    sqrt1 = sqrt0 - p*as[i]*(radius-r0)
                    vsi = np.sqrt(1-sqrt1**2)/p
                    zd = (vsi-vs[i])/as[i] + h[i]
                r0 += dr
            p += 0.0003
        while m ==1 and p < 0.0003:
            double_p = p**2
            r0 = 0
            for i in range(i1):
                vsi = vs[i] + as[i]*hi[i]
                vspp0 = vs[i]**2*double_p
                vspp1 = vsi**2*double_p
                sqrt0 = np.sqrt(1-vspp0)
                sqrt1 = np.sqrt(1-vspp1)
                sqinv = 1/(sqrt0+sqrt1)
                aux = hi[i]*(vs[i]*vsi)*sqinv
                r0 += aux*p
    elif nc == 2:
        double_p = p**2
        hz = 0
        aux = 1/p
        for i in range(num_layer-1):
            hz = h[i]
            if aux > vs[i] and aux <= vs[i+1]:
                n = i
                hz += (aux-vs[i])/as[i]
                for j in range(n-1):
                    hi[j] = h[j+1] - h[j]
        print("Error in ray tracing....")
        hi[n] = hz - h[n]
        r0 = 0
        for i in range(n-1):
            vsi = vs[i] + as[i]*hi[i]
            sqrt0 = np.sqrt(1-vs[i]**2*double_p)
            sqrt1 = np.sqrt(1-vsi**2*double_p)
            sqinv = 1/(sqrt0+sqrt1)
            aux = hi[i]*(vs[i]*vsi)*sqinv
            df = aux*p
            if r0+dr <= radius:
                sqrt1 = sqrt0 - p*as[i]*(radius-r0)
                vsi = np.sqrt(1-sqrt1**2)/p
                zd = (vsi-vs[i])/as[i] + h[i]
            r0 += dr

            if r0+dr >= radius:
                sqrt0 = p*as[i]*(radius-r0)
                vsi = np.sqrt(1-sqrt0**2)/p
                sqrt11 = sqrt0
                zd = (vsi-vs[n])/as[n] + h[n]
            r0 += dr

            for i in range(n-1, 0, -1):
                vsi = vs[i] + as[i]*hi[i]
                sqrt0 = np.sqrt(1-vs[i]**2*double_p)
                sqrt1 = np.sqrt(1-vsi**2*double_p)
                sqinv = 1/(sqrt0+sqrt1)
                aux = hi[i]*(vs[i]*vsi)*sqinv
                df = aux*p
                if r0+dr >= radius:
                    sqrt0 = p*as[i]*(radius-r0) + sqrt1
                    vsi = np.sqrt(1-sqrt0**2)/p
                    zd = (vsi-vs[i])/as[i] + h[i]
                r0 += dr
            print("Error in ray tracing...")
    return p,zd,sz,i1,r,m,nc,sqrt11

def greens_function(theta, phi, bx, by, bz, cx, cy):
    amou = (1.7+0.2*vp[1]*vs[1]**2)
    cos_phi = np.cos(phi)
    sin_phi = np.sin(phi)

    sin_theta = np.sin(theta)
    cos_theta = np.cos(theta)

    sin_theta2 = np.sin(2*theta)
    cos_theta2 = np.cos(2*theta)
    cc = complex(vs[1]**2/vp[1]**2 - sin_theta**2, 0)
    cc = complex(np.sqrt(cc))
    pr = 0.5/(PI*amou)
    cd = cos_theta*pr/(cos_theta2**2+2*sin_theta2*sin_theta*cc)

    by = cos_theta2*cd
    bx = by*cos_phi
    by = by*sin_phi
    bz = -2*sin_theta*cc*cd

    cx = -sin_phi*pr
    cy = cos_phi*pr

    return theta, phi, bx, by, bz, cx, cy



def coefficient_fps(p, vp1, vs1, ro1, vp2, vs2, r02, ncode, rmod, rph):
    '''
    ----------------------------------------------------------------

     THE ROUTINE COEF8 IS DESIGNED FOR THE COMPUTATION OF REFLECTION
     AND TRANSMISSION COEFFICIENTS AT A PLANE INTERFACE BETWEEN TWO
     HOMOGENEOUS SOLID HALFSPACES OR AT A FREE SURFACE OF A HOMOGENEOUS
     SOLID HALFSPACE.

     THE CODES OF INDIVIDUAL COEFFICIENTS ARE SPECIFIED BY THE
     FOLLOWING NUMBERS
     A/ INTERFACE BETWEEN TWO SOLID HALFSPACES
     P1P1...1       P1S1...2       P1P2...3       P1S2...4
     S1P1...5       S1S1...6       S1P2...7       S1S2...8
     B/ FREE SURFACE (FOR RO2.LT.0.00001)
     double_p..1    PX.....5       PX,PZ...X- AND Z- COMPONENTS OF THE
     PS.....2       PZ.....6       COEF.OF CONVERSION,INCIDENT P WAVE
     SP.....3       SX.....7       SX,SZ...X- AND Z- COMPONENTS OF THE
     SS.....4       SZ.....8       COEF.OFCCONVERSION,INCIDENT S WAVE

     I N P U T   P A R A M E T E R S
           P...RAY PARAMETER
           VP1,VS1,RO1...PARAMETERS OF THE FIRST HALFSPACE
           VP2,VS2,RO2...PARAMETERS OF SECOND HALFSPACE. FOR THE FREE
                    SURFACE TAKE RO2.LT.0.00001,EG.RO2=0., AND
                    ARBITRARY VALUES OF VP2 AND VS2
           NCODE...CODE OF THE COMPUTED COEFFICIENT

     O U T P U T   P A R A M E T E R S
           RMOD,RPH...MODUL AND ARGUMENT OF THE COEFFICIENT

     N O T E S
     1/ POSITIVE P...IN THE DIRECTION OF PROPAGATION
     2/ POSITIVE S...TO THE LEFT FROM P
     3/ TIME FACTOR OF INCIDENT WAVE ... EXP(-I*OMEGA*T)
     4/ FORMULAE ARE TAKEN FROM CERVENY ,MOLOTKOV, PSENCIK, RAY METHOD
        IN SEISMOLOGY, PAGES 30-35. DUE TO THE NOTE 2, THE SIGNS AT
        CERTAIN COEFFICIENTS ARE OPPOSITE

                                            WRITTEN BY V.CERVENY,1976
    ---------------------------------------------------------------
    '''
    if ro2 < 0.000001:
    ## Earth's surface, complex coefficients and coefficient of conversion
        a1 = vs1*p
        a2 = a1**2
        a3 = 2*a2
        a4 = 2*a1
        a5 = 2*a4
        a6 = 1-a3
        a7 = 2*a6
        a8 = 2*a3*vs1/vp1
        a9 = a6**2
        double_d[0] =  p*vp1
        double_d[1] =  p*vs1
        for i in range(2):
            double_d[i] = np.sqrt(np.absolute(1-double_d[i]**2))
            if double_d[i] <= 1:
                s1 = d[1]*d[2]
                s2 = a8*s1
                s = 1/(a9+s2)
                if ncode == 1:
                    r = (-a9+s2)*s
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 2:
                    r = a5*d[1]*s*a6
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 3:
                    r = a5*d[2]*s*a6*vs1/vp1
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 4:
                    r = (s2-a9)*s
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 5:
                    r = a5*s1*s
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 6:
                    r = a7*d[2]*s
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 7:
                    r = a7*d[2]*s
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
                elif ncode == 8:
                    r = -a5*vs1*s1*s/vp1
                    if r < 0:
                        rmod = -r
                        rph = PI
                    rmod = r
                    rph = 0
            elif double_d[i] > 1:
                b[i] = complex(0, d[i])
                h1 = b[1]*b[2]
                h2 = h1*a8
                h = 1/(a9+h2)
                if ncode == 1:
                    double_r = (-a9+h2)*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 2:
                    double_r = a5*b[1]*h*a6
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 3:
                    double_r = a5*b[2]*h*a6*vs1/vp1
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 4:
                    double_r = -(a9-h2)*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 5:
                    double_r = a5*h1*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 6:
                    double_r = a7*b[1]*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 7:
                    double_r = a7*b[2]*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
                elif ncode == 8:
                    double_r = -a5*vs1*h1*h/vp1
                    double_r = a7*b[1]*h
                    z2 = double_r.real
                    z3 = double_r.imag
                    if z2 ==0 and z3 ==0:
                        rmod = 0
                        rph = 0
                    rmod = np.sqrt(z2**2+z3**2)
                    rph = np.arctan2(z3, z2)
            b[i] = complex(d[i], 0)
            h1 = b[1]*b[2]
            h2 = h1*a8
            h = 1/(a9+h2)
            if ncode == 1:
                double_r = (-a9+h2)*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 2:
                double_r = a5*b[1]*h*a6
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 3:
                double_r = a5*b[2]*h*a6*vs1/vp1
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 4:
                double_r = -(a9-h2)*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 5:
                double_r = a5*h1*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 6:
                double_r = a7*b[1]*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 7:
                double_r = a7*b[2]*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 8:
                double_r = -a5*vs1*h1*h/vp1
                double_r = a7*b[1]*h
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            
    prmt[0] = vp1
    prmt[1] = vs1
    prmt[2] = vp2
    prmt[3] = vs2

    a1 = vp1*vs1
    a2 = vp2*vs2
    a3 = vp1*ro1
    a4 = vp2*ro2
    a5 = vs1*ro1
    a6 = vs2*ro2

    q = 2*(a6*vs2 - a5*vs1)
    double_p = p**2
    qp = q*double_p
    x = ro2 - qp
    y = ro1 + qp
    z = ro2 -ro1 - qp
    g1 = a1*a2*double_p*z**2
    g2 = a2*x**2
    g3 = a1*y**2
    g4 = a4*a5
    g5 = a3*a6
    g6 = q**2*double_p

    for i in range(4):
        double_d[i] = p*prmt[i]
        d[i] = np.sqrt(np.absolute(1-double_d[i]**2))
    ##if double_d[1] <= 1 and double_d[2] and double_d[3] <= 1 and double_d[4] 
    for j in range(4):
        if double_d[i] <= 1:
        ### Real coefficient
            e1 = d[1]*d[2]
            e2 = d[3]*d[4]
            e3 = d[1]*d[4]
            e4 = d[2]*d[3]
            s1 = g1
            s2 = g2*e1
            s3 = g3*e2
            s4 = g4*e3
            s5 = g5*e4
            s3 = g6*e1*e2
            s = 1/(s1+s2+s3+s4+s5+s6)
            sb = 2*s
            sc = sb*p
            if ncode == 1:
                r = s*(s2+s4+s6-s1-s3-s5)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 2:
                r = vp1*d[1]*sc*(q+y+e2+a2*x*z)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 3:
                r = a3*d[1]*sb*(vs2*d[2]*x + vs1*d[4]*y)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 4:
                r = -a3*d[1]*sc*(q*e4-vs1*vp2*z)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 5:
                r = -vs1*d[2]*sc*(q*y*e2 + a2*z*x)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 6:
                r = s*(s2+s5+s6-s1-s3-s4)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 7:
                r = a5*d[2]*sc*(q*e3-vp1*vs2*z)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
            elif ncode == 8:
                r = a5*d[2]*sb*(vp*d[3]*y + vp2*d[1]*x)
                if r < 0:
                    rmod = -r
                    rph = PI
                rmod = r
                rph = 0
    # complex coefficients
    for i in range(4):
        if double_d[i] > 1:
            b[i] = complex(d[i], 0)
            c1 = b[1]*b[2]
            c2 = b[3]*b[4]
            c3 = b[1]*b[4]
            c4 = b[2]*b[3]
            h1 = g1
            h2 = g2*c1
            h3 = g3*c2
            h4 = g4*c3
            h5 = g5*c4
            h6 = g6*c1*c2
            h = 1/(h1+h2+h3+h4+h5+h6)
            hb = 2*h
            hc = hb*p
            if ncode == 1:
                double_r = h*(h2+h4+h6-h1-h3-h5)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 2:
                double_r = vp1*b[1]*hc*(q*y*c2 + a2*x*z)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 3:
                double_r = a3*b[1]*hb*(vs2*b[2]*x + vs1*b[4]*y)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 4:
                double_r = -a3*b[1]*hc*(q*c4 - vs1*vp2*z)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 5:
                double_r = -vs1*b[2]*hc*(q*y*c2 + a2*x*z)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 6:
                double_r = h*(h2+h5+h6-h1-h3-h4)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 7:
                double_r = a5*b[2]*hc*(q*c3 - vp1*vs2*z)
                z2 = double_r.real
                z3 = double_r.imag
                if z2 ==0 and z3 ==0:
                    rmod = 0
                    rph = 0
                rmod = np.sqrt(z2**2+z3**2)
                rph = np.arctan2(z3, z2)
            elif ncode == 8:
                double_r = a5*b[2]*hb*(vp1*b[3]*y + vp2*b[1]*x)
                double_r = a5*b[2]*h
    return p, vp1, vs1, ro1, vp2, vs2, r02, ncode, rmod, rph

def coefficient_fsh(p, vp1, vs1, ro1, vp2, vs2, r02, ncode, rmod, rph):
    '''
   ----------------------------------------------------------------
     THE CODES OF INDIVIDUAL COEFFICIENTS ARE SPECIFIED BY THE
     FOLLOWING NUMBERS
     A/ INTERFACE BETWEEN TWO SOLID HALFSPACES
     S1S1...1       S1S2...2

     Input parameters:
           p.............RAY PARAMETER
           vp1,vs1,ro1...PARAMETERS OF THE FIRST HALFSPACE
           vp2,vs2,ro2...PARAMETERS OF SECOND HALFSPACE
           ncode.........CODE OF THE COMPUTED COEFFICIENT

     Output parameters:
           rmod,rph......MODUL AND ARGUMENT OF THE COEFFICIENT
   ---------------------------------------------------------------
    '''
    p1 = p*vs1
    p1 = np.sqrt(1-p1**2)*vs1*ro1
    p2 = p*vs2
    if np.absolute(p2) >= 1:
        p2 = np.sqrt(p2**2-1)*vs2*ro2
        if ncode == 1:
            rmod = 1
            rph = -2*np.arctan2(p2, p1)
        elif ncode == 2:
            print("The trasmitted wave don't exist...!")
    p2 = np.sqrt(1-p2**2)*vs2*ro2
    if ncode == 1:
        rmod = (p1-p2)/(p1+p2)
        rph = 0
    elif ncode == 2:
        rmod = 2*p1/(p1+p2)
        rph = 0
    return p, vp1, vs1, ro1, vp2, vs2, r02, ncode, rmod, rph


    

def ray_tracing(observe_x_direction, observe_y_direction, rupture_time, g 
            ,area_xy, index_comp):
    '''
    ---------------------------------------------------------------
    This program creates the source coordinate and calculates ray     
    tracing in a 3-D vertical inhomogeneous media for an arbitrary    
    rupture source.                                                                                                                    
                    Written by Yuehue Zeng at USC, sept. 11, 1988  
    ---------------------------------------------------------------
    '''
    ds = (d2-d1)/(gridnum_width-1)
    err = 0.01*ds

## Create global source coordinate, and calculate fault normal and slip vector
    for i in range(1, n-2):
        ap = (vp[i+1] - vp[i])/(h[i+1] - h[i])
        as = (vs[i+1] - vp[i])/(h[i+1] - h[i])
    ds_y = ds*np.cos(theta)
    ds_z = ds*np.sin(theta)

    sy[1] = -(dso-d1)*np.cos(theta)
    sz[1] = -d1*np.sin(theta)
    for i in range(2,gridnum_length):
        sy[i] = dy + sy[i-1]
        sz[i] = dz + sz[i-1]
    vector_n2 = sin(theta)
    vector_n3 = -cos(theta)
    vector_m1 = cos(phi)
    vector_m2 = -cos(theta)*sin(phi)
    vector_m3 = -sin(theta)*sin(phi)
## Ray tracing
    m1 = 1
    mj = 1
    x = sx[1] - observe_x_direction
    y = sy[gridnum_length]-observe_y_direction
    radius = np.sqrt(x**2 + y**2 + sz[gridnum_length**2])
    p = 0.5*np.sqrt(r**2 + sz[gridnum_length])/(r*vs[1])

## Loop for the 2 points ray tracing between observation and all source points
    for i in range(1, gridnum_width):
        m2 = m1 + gridnum_length - 1
        mj = -mj
        if mj == 1 :
            m11 = m1
            m22 = m2
        elif mj == -1:
            m11 = m2
            m22 = m1
        x = sx[i] - observe_x_direction
        double_x = X**2
        for j in range(m11, m22, mj):
            l = j-m1+1
            y = sy[l] - observe_y_direction
            radius = np.sqrt(double_x + y**2)

    ### Calculate phi_0
            if radius == 0:
                phi_0 = PI/2
            else:
                phi_0 = np.arctan2(y,x)
    
            for k in range(1, n):
                if h[k] > sz[l]:
                    i1 = k - 1
    ### Interation for 2 points ray tracing
        iteration = -1
        nc = 0
        z1 = h[n]
        z2 = h[1]
        ray_path1(p, zd, zp, sz[l], i1, r0, rp, radius, 1, nc, aux, pn)

        iteration += 1
        if zx > sz[l]:
            p2 = p
            aux2 = aux
            z2 = zd
        else:
            p1 = p 
            aux1 = aux
            z1 = zd
        if iteration > 10:
            if z2 > sz[l] and z1 < sz[l]:
                if zd > sz[l]:
                    p2 = p 
                    aux2 = aux
                    z2 = zd
                else:
                    p1 = p 
                    aux1 = aux
                    z1 = zd
                p = 0.5*d0*(p1+p2)
            if z2 < sz[l]:
                p -= 0x00001
                ray_path1(p, zd, zp, sz[l], i1, r0, rp, radius, 1, nc, aux, pn)
            else:
                p += 0x00001
                ray_path1(p, zd, zp, sz[l], i1, r0, rp, radius, 1, nc, aux, pn)

            if p >= 0.0003:
                delta = sz[l] - zd
            else:
                delta = radius - r0
                zp = rp
            if np.absolute(delta) < err:
                ray_path1(p, zd, zp, sz[l], i1, r0, rp, radius, 2, nc, aux, pn)
                theta_0 = np.arcsin(p*vs[1])
                greens_function(theta_0, phi_0, bx, by, bz, cx, cy)
            emp = delta/zp
            p += tmp

            if p <= 0 :
                p = (p-tmp)/2
                ray_path1(p, zd, zp, sz[l], i1, r0, rp, radius, 1, nc, aux, pn)

            if zd > sz[l]:
                p2 = p 
                aux2 = aux
                z2 = zd
            else:
                p1 = p
                aux1 = aux
                z1 = zd
            p = 0.5*d0*(p1 + p2)
            ray_path2(p, zd, sz[l], i1, radius, 1, nc, aux)
            iteration += 1
            delta = sz[l] - zd
            if np.absolute(delta) < err:
                ray_path1(p, zd, zp, sz[l], i1, t, rp, radius, 2, nc, aux, pn)
                theta_0 = np.arcsin(p*vs[1])
                greens_function(theta_0, phi_0, bx, by, bz, cx, cy)
            if zd > sz[l]:
                p2 = p
                aux2 = aux
                z2 = zd
            else:
                p1 = p
                aux2 = aux
                z1 = zd

            if iteration > 40 and (z2-z1) < ds:
                ray_path1(p1, zd, zp1, sz[l], i1, t1, rp, radius, 2, nc, aux1, pn)
                ray_path1(p2, zd, zp2, sz[l], i1, t2, rp, radius, 2, nc, aux2, pn)
                aux1 = z1/(z1+z2)
                aux2 = z2/(z1+z2)
                p = p1*aux2 + p2*aux1
                zp = zp1*aux2 + zp2*aux1
                t = t1*zux2 + t2*aux1
                theta_0 = np.arcsin(p*vs[1])
                greens_function(theta_0, phi_0, bx, by, bz, cx, cy)
            p = 0.5*d0*(p1 + p2)
            ray_path2(p, zd, sz[l], i1, radius, 1, nc, aux)

            ### Calculation of trasmission coefficients
            rmod1 = 1
            rph1 = 0
            rmod2 = 1
            rph2 = 0
            for i in range(1, i1-1):
                hi = h[i+1]-h[i]
                vp1 = vp[i] + ap[i]*hi
                vs1 = vs[i] + as[i]*hi
                ro1 = 1.7 + 0.2*vp1
                vp2 = vp[i+1]
                vs2 = vs[i+1]
                ro2 = 1.7 + 0.2*vs2
                coefficient_fps(p, vp1, vs1, ro1, vp2, vs2, ro2, 8, rm1, rp1)
                coefficient_fsh(p, vp1, vs1, ro1, vp2, vs2, ro2, 2, rm2, rp2)
                rmo1 = rmod1*rm1
                rph1 += rp1
                rmod2 = rmod2*rm2
                rph2 += rp2

            coefficient_1 = rmod1*complex(np.cos(rph1), np.sin(rph1))
            coefficient_2 = rmod2*complex(np.cos(rph2), np.sin(rph2))
        ### Calculate vector or ray tangent and normals
            hi = sz[l] - h[i1]
            ro1 = 1.7 + 0.2*(vp[i1] + ap[i1]*hi)
            ro0 = 1.7 + 0.2*vp[1]
            vs1 = vs[i1] + as[i1]*hi
            amou1 = ro1*vs1*vs1
            sinth0 = p*vs1
            costh0 = np.sqrt(1-sinth0**2)
            s1 = -np.sin(phi0)
            s2 = np.cos(phi0)
            r1 = s2*sinth0
            r2 = -s1*sinth0
            t1 = r3*s2
            t2 = -r3*s1
            t3 - -sinth0
        ### Calculate the arrival time and the Green function response
            x1 = (t2*vector_n2 + t2*vector_n3)*(r1*vector_m1 + r2*vector_m2 + 
                r3*vector_m3) + (t1*vector_m1 + t2*vector_m2 + t3*vector_m3)*
                (r2*vector_n2 + r3*vector_n3)
            
            cs = complex(1,0)
            if r != 0 :
                if zp < 0 : ### ray passes through focus point when zp < 0
                    cs = complex(0, 1)
                    zp = -zp
                ss = amou1/vs1*np.sqrt(ro0*(vs[1]**2)*p/(ro1*vs1*zp*radius))
            else:
                ss = amou1/vs1*np.sqrt(ro0*(vs[1])*p/(ro1*vs1*zp*zp))
            cs = cs*ss
            rupture_time[j] += t
            if index_comp == 1:
                x22 = s2*vector_n2*(r1*vector_m1 + r2*vector_m2 + r3*vector_m3) + 
                    (s1*vector_m1 + s2*vector_m2)*(r2*vector_n2 + r3*vector_n3)
                bx = bx*coefficient_1
                coefficient_1 = cx*coefficient_2
                g[j] = -cs*(x1*bx + x22*coefficient_1)
            elif index_comp == 2:
                x22 = s2*vector_n2*(r1*vector_m1 + r2*vector_m2 + r3*vector_m3) + 
                (s1*vector_m1 + s2*vector_m2)*(r2*vector_n2 + r3*vector_n3)
                by = by*coefficient_1
                coefficient_2 = cy*coefficient_2
                g[j] = -cs*(x1*by + x22*coefficient_2)
            elif index_comp == 3:
                bz = bz*coefficient_1
                g[j] = -(cs*x1)*bz
    return observe_x_direction, observe_y_direction, rupture_time, g 
            ,area_xy, index_comp

def xdt(x, y, t21, t23, dxdt1, dxdt2, dxdt3):

    if x == 0:
        dxdt1 = 0
        dxdt2 = 0
        dxdt3 = 0
    elif (y ==0 and x!= 1) or x==1 and np.absolute(t21) >= np.absolute(t23):
        dxdt1 = 1/t21
        dxdt2 = -x*dxdt1
        dxdt1 = -dxdt1-dxdt2
        dxdt3 = 0
    else:
        dxdt1 = 0
        dxdt3 = 1/t23
        dxdt2 = -x*dxdt3
        dxdt3 = -dxdt3-dxdt2

    return x, y, t21, t23, dxdt1, dxdt2, dxdt3

def derivatives(t, syn, ads1, ads2, ads3, adt1, adt2, adt3):
    t013 = (t-t1)*at31
    t033 = (t-t3)*at31
    a1 = g1 + g3*t013
    b1 = s1 + s2*t013
    x12 = x1**2
    x22 = x2**2
    x = x2 - x1
    double_x = (x22-x12)/2
    triple_x = (x22*x2-x12*x1)/3

    aa1 = a1*x + a1*double_x
    aa2 = a1*double_x + a2*triple_x
    syn = aa1*b1 + aa2*b2

    ads1 = aa1*(1-t013) + aa2*(t213-1)
    ads2 = aa2
    ads3 = aa1*b1 + aa2*t213

    adt1 = syn + s3*(aa1*t033-aa2*t233)
    adt2 = s3*aa2
    adt3 = -syn - s3*ads3

    bb1 = b1*x + b2*double_x
    bb2 = b1*double_x + b2*triple_x
    adt1 = (adt1+g3*(bb1*t033-bb2*t233))*at31
    adt2 = (adt2-g3*bb2)*at31
    adt3 = (adt3-g3*(bb1*t013-bb2*t213))*at31

    xdt(x2, y2, t21, t23,x, double_x, triple_x)
    aa1 = (a1+a2*x1) * (b1+b2*x1)
    adt1 -= aa1*X
    adt2 -= aa1*double_x
    adt3 -= aa1*triple_x
    
    return t, syn, ads1, ads2, ads3, adt1, adt2, adt3


def contour_values(n, time, xt, yt, n1, n2, rupture_time, area_xy, gridnum_width,
     gridnum_length):
    # Find the position of the contour values at each grid
    k1 = 1
    ij = 0
    ij2 = 0
    notex = True
    double_n = 0
    for i in range(gridnum_width-1):
        notey = notex
        for j in range(gridnum_length-1):
            ij += 1
            ij2 += 2
            ij1 = ij2 - 1
            t11 = rupture_time[ij]
            t12 = rupture_time[ij+1]
            t21 = rupture_time[ij+gridnum_length]
            t22 = rupture_time[ij+gridnum_length+1]
            if notey:
                t1 = t12
                t2 = t22
                t3 = t11
                time_max = max(t1, t2, t3)
                time_min = min(t1, t2, t3)
                if k1 > n:
                    k1 = n
                if t[k1] < tmin:
                    for k in range(k1+1,n):
                        if time_min < t[k]:
                            k1 = k
                        else:
                            for k in range(k1, 1, -1):
                                if t[k] < time_min:
                                    k1 = k + 1
                            k1 = k + 1
                for k in range(k1, n):
                    if time_max < t[k]:
                        k2 = k - 1
                        n1[ij1] = k1
                        n2[ij1] = k2
                        if k1 <= 1:
                            for k in range(k1, k2):
                                t1k = t1 - t[k]
                                t2k = t2 - t[k]
                                t3k = t3 - t[k]
                                np = 0
                                if t1k*t2k < 0:
                                    np += 1
                                    double_n += 1
                                    xt(double_n) = t1k/(t1-t2)
                                    yt(double_n) = 0
                                if t1*t3k < 0:
                                    np += 1
                                    double_n += 1
                                    xt(double_n) = 0
                                    yt(double_n) = t1k/(t1-t3)
                                    if np == 2:
                                        if k2 >= n:
                                            t1 = t21
                                            t2 = t11
                                            t3 = t22
                                            time_max = max(t1, t2, t3)
                                            time_min = min(t1, t2, t3)
                                            if k1 > n:
                                                k1 = n
                                            if t[k1] < time_min:
                                                for k in range(k1+1, n):
                                                    if time_min < t[k]:
                                                        k1 = k
                                                    else:
                                                        for k in range(k1, 1, -1):
                                                            if t[k] <= time_min:
                                                                k1 = k + 1
                                                        k1 = k + 1
                                                k2 = k - 1
                                                n1[ij1] = k1
                                                n2[ij1] = k2

                                                if k1 <= 1:
                                                    for k in range(k1, k2):
                                                        t1k = t1 - t[k]
                                                        t2k = t2 - t[k]
                                                        t3k = t3 - t[k]
                                                        np = 0
                                                        if t1k*t2k < 0:
                                                            np += 1
                                                            double_n += 1
                                                            xt(double_n) = 0
                                                            yt(double_n) = t1k/(t1-t3)
                                                            if np == 2:
                                                                if k2 >= n:
                                                                    t1 = t22
                                                                    t2 = t12
                                                                    t3 = t21
                                                                    time_max = 
                                                                        max(t1, t2, t3)
                                                                    time_min = 
                                                                        min(t1, t2, t3)
                                                                    if k1 > n:
                                                                        k1 = n
                                                                    if t[k1] < time_min:
                                                                        for k in range(k1+1, n):
                                                                            if time_min < t[k]:
                                                                                k1 = k
                                                                            else:
                                                                                for k in range(k1, 1, -1):
                                                                                    if t[k] >= time_min:
                                                                                        k1 = k + 1
                                                                                    k1 = k + 1
                                                                            for k in range(k1, n):
                                                                                if time_max <= t[k]:
                                                                                    k2 = k - 1
                                                                                    n1[ij2] = k1
                                                                                    n2[ij2] = k2
                                                                                    if k1 <= 1 :
                                                                                        for k in range(k1, k2):
                                                                                            t1k = t1 - t[k]
                                                                                            t2k = t2 - t[k]
                                                                                            t3k = t3 - t[k]
                                                                                            np = 0
                                                                                            if t1k*t2k < 0:
                                                                                                np += 1
                                                                                                double_n += 1
                                                                                                xt(double_n) = t1k/(t1-t2)
                                                                                                yt(double_n) = 0
                                                                                            elif t1k*t3k < 0:
                                                                                                np += 1
                                                                                                double_n += 1
                                                                                                xt(double_n) = 0
                                                                                                yt(double_n) = t1k/(t1-t3)
                                                                                                if np == 2 :
                                                                                                    if t1 == t[k2+1] and t2 == t[k2+1]
                                                                                                        and k2 >= n:
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 0
                                                                                                        yt[double_n] = 0
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 1
                                                                                                        yt[double_n] = 0
                                                                                                        n2[ij2] += 1
                                                                                                notey != notey
                                                                                                ij += 1
                                                                                                notex != notex
                                                                                            if t2k*t3k < 0:
                                                                                                np += 1
                                                                                                double_n += 1
                                                                                                xt(double_n) = t3k/(t3-t2)
                                                                                                yt(double_n) = t2k/(t2-t3)
                                                                                                if np == 2:
                                                                                                    if t1 == t[k2+1] and t2 == t[k2+1]
                                                                                                        and k2 >= n:
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 0
                                                                                                        yt[double_n] = 0
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 1
                                                                                                        yt[double_n] = 0
                                                                                                        n2[ij2] += 1
                                                                                                notey != notey
                                                                                                ij += 1
                                                                                                notex != notex
                                                                                            if t2k == 0:
                                                                                                np += 1
                                                                                                double_n += 1
                                                                                                xt(double_n) = 1
                                                                                                yt(double_n) = 0
                                                                                                 if np == 2:
                                                                                                    if t1 == t[k2+1] and t2 == t[k2+1]
                                                                                                        and k2 >= n:
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 0
                                                                                                        yt[double_n] = 0
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 1
                                                                                                        yt[double_n] = 0
                                                                                                        n2[ij2] += 1
                                                                                                notey != notey
                                                                                                ij += 1
                                                                                                notex != notex
                                                                                            ## contnue at fortran file line-1573    
                                                                                            if t3k == 0:
                                                                                                 np += 1
                                                                                                double_n += 1
                                                                                                xt(double_n) = 0
                                                                                                yt(double_n) = 1
                                                                                                if np == 2:
                                                                                                    if t1 == t[k2+1] and t2 == t[k2+1]
                                                                                                        and k2 >= n:
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 0
                                                                                                        yt[double_n] = 0
                                                                                                        double_n += 1
                                                                                                        xt[double_n] = 1
                                                                                                        yt[double_n] = 0
                                                                                                        n2[ij2] += 1
                                                                                                notey != notey
                                                                                                ij += 1
                                                                                                notex != notex
                                                                                            f_060.write("Error in the data/program !")
    return n, time, xt, yt, n1, n2, rupture_time, area_xy, gridnum_width, gridnum_length

def synth_inversion(observe_x_direction, observe_y_direction, double_n, n, time,
 synthetic, nc1, nc2, nc3, adt, adti, ads, adsi, index_comp, ihb):
    error = 0.00001
    area_xy = gridnum_width*gridnum_length
    ray_tracing(observe_x_direction, observe_y_direction, rupture_time, g 
            ,area_xy, index_comp)
    contour_values(n, time, xt, yt, n1, n2, rupture_time, area_xy, gridnum_width,
     gridnum_length)
    
    ## Calculate the derivatives of seismograms with respect to time and s
    for i in range(area_xy):
        nc1[i] = 0
        nc2[i] = 0
        ij = 0
        ij2 = 0
        notex = True
        for i in range(gridnum_width-1):
            notey = notex
            for j in range(gridnum_length-1):
                ij += 1
                ij2 += 2
                ij1 = ij2 - 1
                if notey:
                    nmin = int(min(n1[ij1], n1(ij2)))
                    nmax = int(max(n2[ij1], n2(ij2)))

                    if nmin < nc1[ij] or nc1[ij] == 0:
                        nc1[ij] = nmin
                    if nmax > nc2[ij] :
                        nc2[ij] = nmax
                    
                    ija = ij + 1
                    if n1[ij1] < nc1[ija] or nc1[ija] == 0:
                        nc1[ija] = n1[ij1]
                    if n2[ij2] > nc2[ija]:
                        nc2[ija] = n2[ij1]
                    
                    ija = ij + gridnum_length
                    if n1[ij2] < nc1[ija] or nc1[ija] == 0:
                        nc1[ija] = n1[ij2]
                    if n2[ij2] > nc2[ija]:
                        nc2[ija] = n2[ij2]

                    ija += 1
                    if nmin < nc1[ija] or nc1[ij] == 0:
                        nc1[ija] = nmin
                    if nmax > nc2[ija]:
                        nc2[ija] = nmax
                else:
                    nmin = int(min(n1[ij1], n1(ij2)))
                    nmax = int(max(n2[ij1], n2(ij2)))

                    if n1[ij1] < nc1[ij] or nc1[ij] == 0:
                        nc1[ij] = n1[ij1]          
                    if n2[ij1] > nc2[ij]:
                        nc2[ij] = n2[ij1]
                    
                    ija = ij + 1
                    if nmin < nc1[ija] or nc1[ija] == 0:
                        nc1[ija] = nmin
                    if nmax > nc2[ija]:
                        nc2[ija] = nmax
                    
                    ija = ij + gridnum_length
                    if nmin < nc1[ija] or nc1[ija] == 0:
                        nc1[ija] = nmin
                    if nmax > nc2[ija] :
                        nc2[ija] = nmax
                    
                    ija += 1
                    if n1[ij2] < nc1[ija] or nc1[ija] == 0:
                        nc1[ija] = n1[ij2]
                    if n2[ij2] > nc2[ija]:
                        nc2[ija] = n2[ij2]
                    
                    notey != notex
                ij += 1
                notex != notey
            double_n = 1
            for i in range(area_xy):
                nc2[i] = nc2[i] - nc1[i]
                nc3[i] = double_n
                double_n += nc2[i]

            double_n -= 1
            for i in range(double_n):
                adt[i] = 0
                adti[i] = 0
                ads[i] = 0
                adsi[i] = 0
            for i in range(n):
                synr[i] = 0
                syni[i] = 0
            n3 = 0
            ij = 0
            ij2 = 0
            notex = True
            for i in range(gridnum_width-1):
                notey = notex
                for i in range(gridnum_length-1):
                    ij += 1
                    ij2 += 2
                    ij1 = ij2 -1

                    m11 = ij
                    m12 = m11 + 1
                    m21 = m11 + gridnum_length
                    m22 = m21 + 1

                    if notey:
                        l1 = m12
                        l2 = m22
                        l3 = m11
                        l4 = 1
                        l5 = 1
                        if np.absolute(rupture_time(l2)-rupture_time(l1)) > 
                            np.absolute(rupture_time[l3]-rupture_time[l1]):
                            ija = l2
                            l2 = l3
                            l3 = ija
                            l4 = 0
                        t1 = rupture_time[l1]
                        t2 = rupture_time[l2]`
                        t3 = rupture_time[l3]
                        g1 = g[l1]
                        g2 = g[l2] - g1
                        g3 = g[l3] - g1
                        s1 = s[l1]
                        s2 = s[l2] - s1
                        s3 = s[l3] - s1

                        at31 = 1/(t3-t1)
                        t21 = t2-t1
                        t213 = t21*at31
                        t23 = t2 - t3
                        t233 = t23*at31
                        a2 = g2 - g3*t213
                        b2 = s2 - s3*t213

                        for k in range(n1[ij1], n2[ij1]):
                            if l4 == 1:
                                n3 += 2
                                x1 = xt(n3-1)
                                x2 = xt(n3)
                                y1 = yt(n3-1)
                                y2 = yt(n3)
                            else:
                                n3 += 2
                                x1 = yt(n3-1)
                                x2 = yt(n3)
                                y1 = xt(n3-1)
                                y2 = xt(n3)

                            derivatives(t(k),syn1,ads1,ads2,ads3,adt1,adt2,adt3)

                            c = np.copysign(at31, x2-x1)
                            synr[k] += c*syn1.real
                            syni[k] += x*syn1.imag
                            ija = nc3[l1] + k -nc1[l1]
                            ads[ija] += c*ads1.real
                            adsi[ija] += c*ads1.imag
                            adt[ija] += c*adt1.real
                            adti[ija] += c*adt1.imag
                            ija = nc3[l2] + k - nc1[l2]
                            ads[ija] += c*ads2.real
                            adsi[ija] += c*ads2.imag
                            adt[ija] += c*adt2.real
                            adti[ija] += c*adt2.imag
                            ija = nc3[l3] + k - nc1[l3]
                            ads[ija] += c*ads3.real
                            adsi[ija] += c*ads3.imag
                            adt[ija] += c*adt3.real
                            adti[ija] += c*adt3.imag

                        if l5 == 1:
                            l1 = m21
                            l2 = m11
                            l3 = m22
                            l4 = 1
                            l5 = 0
                            ij1 = ij2
                            if np.absolute(rupture_time(l2)-rupture_time(l1)) > 
                            np.absolute(rupture_time[l3]-rupture_time[l1]):
                                ija = l2
                                l2 = l3
                                l3 = ija
                                l4 = 0
                    else:
                        l1 = m11
                        l2 = m21
                        l3 = m12
                        l4 = 1
                        l5 = 1
                        if np.absolute(rupture_time(l2)-rupture_time(l1)) > 
                            np.absolute(rupture_time[l3]-rupture_time[l1]):
                            ija = l2
                            l2 = l3
                            l3 = ija
                            l4 = 0
                        t1 = rupture_time[l1]
                        t2 = rupture_time[l2]`
                        t3 = rupture_time[l3]
                        g1 = g[l1]
                        g2 = g[l2] - g1
                        g3 = g[l3] - g1
                        s1 = s[l1]
                        s2 = s[l2] - s1
                        s3 = s[l3] - s1

                        at31 = 1/(t3-t1)
                        t21 = t2-t1
                        t213 = t21*at31
                        t23 = t2 - t3
                        t233 = t23*at31
                        a2 = g2 - g3*t213
                        b2 = s2 - s3*t213

                        t1 = rupture_time[l1]
                        t2 = rupture_time[l2]`
                        t3 = rupture_time[l3]
                        g1 = g[l1]
                        g2 = g[l2] - g1
                        g3 = g[l3] - g1
                        s1 = s[l1]
                        s2 = s[l2] - s1
                        s3 = s[l3] - s1

                        at31 = 1/(t3-t1)
                        t21 = t2-t1
                        t213 = t21*at31
                        t23 = t2 - t3
                        t233 = t23*at31
                        a2 = g2 - g3*t213
                        b2 = s2 - s3*t213

                        for k in range(n1[ij1], n2[ij1]):
                            if l4 == 1:
                                n3 += 2
                                x1 = xt(n3-1)
                                x2 = xt(n3)
                                y1 = yt(n3-1)
                                y2 = yt(n3)
                            else:
                                n3 += 2
                                x1 = yt(n3-1)
                                x2 = yt(n3)
                                y1 = xt(n3-1)
                                y2 = xt(n3)

                            derivatives(t(k),syn1,ads1,ads2,ads3,adt1,adt2,adt3)

                            c = np.copysign(at31, x2-x1)
                            synr[k] += c*syn1.real
                            syni[k] += x*syn1.imag
                            ija = nc3[l1] + k -nc1[l1]
                            ads[ija] += c*ads1.real
                            adsi[ija] += c*ads1.imag
                            adt[ija] += c*adt1.real
                            adti[ija] += c*adt1.imag
                            ija = nc3[l2] + k - nc1[l2]
                            ads[ija] += c*ads2.real
                            adsi[ija] += c*ads2.imag
                            adt[ija] += c*adt2.real
                            adti[ija] += c*adt2.imag
                            ija = nc3[l3] + k - nc1[l3]
                            ads[ija] += c*ads3.real
                            adsi[ija] += c*ads3.imag
                            adt[ija] += c*adt3.real
                            adti[ija] += c*adt3.imag

                        if l5 == 1:
                            l1 = m21
                            l2 = m11
                            l3 = m22
                            l4 = 1
                            l5 = 0
                            ij1 = ij2
                            if np.absolute(rupture_time(l2)-rupture_time(l1)) > 
                            np.absolute(rupture_time[l3]-rupture_time[l1]):
                                ija = l2
                                l2 = l3
                                l3 = ija
                                l4 = 0
                    notey != notey
                    ij += 1
                    notex != notex

                    ### convolution with the wavelet
                    swave_max = 0
                    for i in range(n):
                        if swave_max < np.absolute(synr[i]):
                            swave_max = np.absolute(synr[i])
                        swave_max *= err
                        if swave_max < np.absolute(syni[i]):
                            ihb = 1
                            for i in range(n):
                                syn[i] = 0
                                for j in range(n):
                                    syn[i] = syn[i] + synr[j]*wavelet[i-j] + 
                                    syni[j]*wavelet[i-j]
                            ihb = 0
                            for i in range(n):
                                syn[i] = 0
                                for j in range(n):
                                    syn[i] += synr[j]*wavelet[i-j]
    return  observe_x_direction, observe_y_direction, double_n, n, time,
 synthetic, nc1, nc2, nc3, adt, adti, ads, adsi, index_comp, ihb

if __name__ == __main__:
##def read_invslp_input(filename):
    with open("invslp.in", 'r') as invslp_file:
        line = invslp_file.readline().replace(" ", "").strip()
        num_iterate = list(line[0])
        damp = list(line[2])
        rns = list(line[3])
        dcor_width = list(line[4])
        dcor_length = list(line[5])

        ##Read the 2nd line
        line = invslp_file.readline().replace(" ", "").strip()
        num = list(line[0])
        twin = list(line[1])
        low_freq_corner = list(line[2])
        low_freq_cutoff = list(line[3])
        high_freq_corner = list(line[4])
        high_freq_cutoff = list(line[5])

        ##Read the 3rd line
        line = invslp_file.readline().replace(" ", "").strip()
        fault_width = list(line[0])
        fault_upper_dip = list(line[1])
        fault_lower_dip = list(line[2])
        hypocenter_width = list(line[3])
        hypocenter_dip = list(line[4])
        theta = list(line[5])
        phi = list(line[6])


        ## Read  how many layers of velocity structure
        num = int(invslp_file.readline())
        ## Read the velocity structure's data
        for j in range(num):
            layer = list(invslp_file.readline().replace(" ","").strip())
        ## Read the lengh and width of the rupture plane
        length_width = list(invslp_file.readline().replace(" ","").strip())
        gridnum_width = length_width[0]
        gridnum_length = length_width[1]
        
    invslp_file.close()

    # Read the observation data
    with open("seis3-1.dat", "r") as f_seis31:
        num = int(f_seis31.readline().strip())/2
        
        ## Creating an empty list for storing seismogram data
        obs_seis_group = []

        for j in range(num):
            '''
            line2 = list(f_seis31.readline().replace(" ","").strip())
            observe_x_direction = line2[0]
            observe_y_direction = line2[1]
            num_timeseries = line2[2]
            index_comp = line2[3]
            dt = line2[4]
            plotting_scale = = line2[5]
            line3 = list(f_seis31.readline().rstrip())
            t = 
            '''
            in_data = in_1.main_prog()
            observe_x_direction = in_data[j][0][0]
            observe_y_direction = in_data[j][0][1]
            num_timeseries = in_data[j][0][2]
            index_comp = in_data[j][0][3]
            dt = in_data[j][0][4]
            plotting_scale = in_data[j][0][5]
            swave_ew = in_data[j][1]
            swave_ns = in_data[j][3]
            ## continue from line 103
            for k in range(len(swave_ew)):
                t[k] = swave_ew[k][0]
                obs_seis[k] = swave_ew[k][1]
                obs_seis[k] *= plotting_scale
                obs_seis_group.append(obs_seis[k])
            for l in range(len(swave_ns)):
                t[l] = swave_ns[l][0]
                obs_seis[l] = swave_ns[l][1]
                obs_seis[l] *= plotting_scale
                obs_seis_group.append(obs_seis[l])
        f_seis31.close()

    ## Create the local source coordinate
    dx = fault_width/(gridnum_width-1)
    sx[0] = -hypocenter_width
    for i in range(1, gridnum_width):
        sx[i] = dx+sx(i-1)
    dy = (fault_lower_dip - fault_upper_dip)/(gridnum_length-1)
    sy[0] = 0
    for j in range(1, gridnum_length):
        sy[j] = dy + sy[j-1]
    dxy = dx * dy
    for i in range(i1):
        plotting_scale[i] *= dxy
        
    ## Read in rupture time and slip density function distributions
    with open ("invo1.dat", "r") as invo_file:
        m1 = 1
        rupture_time_data = []
        slip1_data = []
        slip2_data = []
        finish = ''
        for i in range(gridnum_width):
            for j in range(2):
                time_data = invo_file.readline().rstrip()
                finish += time_data + ' '
            new_data = list(map(int, finish.split()))
            rupture_time_data += new_data
        for k in range(gridnum_width):
            for l in range(2):
                slip_data1 = invo_file.readline().rstrip()
                finish += slip_data1 + ' '
            new_data = list(map(int, finish.split()))
            slip1_data += new_data
        for m in range(gridnum_width):
            for n in range(2):
                slip_data2 = invo_file.readline().rstrip()
                finish += slip_data2 + ' '
            new_data = list(map(int, finish.split()))
            slip2_data += new_data
        invo_file.close()
        
    tmin = rupture_time_data[0]
    imin = 0
    area_xy = gridnum_width*gridnum_length
    area_xy2 = 2*area_xy
    for i in range(1,area_xy):
        if tmin > rupture_time_data[i]:
            tmin = rupture_time_data[i]
            imin = i
# Loop for calculating the synthesis and its derivatives with the model parameters
    wavelet(num_wavelet, diff_time, rise_time, low_freq_corner, hi_freq_corner,
     low_freq_cutoff, hi_freq_cutoff)
    k1 = 1
    l1 = 1
    m1 = 1
    for i in range(i1):
        for j in range(area_xy):
            aux[j] = rupture_time_data[j]
            s[j] = slip1_data[j]
        phi = PI/2

        synth_inversion(observe_x_directio[i], observe_y_direction[i], double_n, n[i], time[k1],
            synthetic[k1], nc1[l1], nc2[l1], nc3[l1], adt[m1], adti[m1], ads[m1], adsi[m1],
            index_comp[i], ihb[i])
        for j in range(area_xy):
            aux[j] = rupture_time_data[j]
            s[j] = slip2_data[j]
        phi = phi0

        synth_inversion(observe_x_directio[i], observe_y_direction[i], double_n, n[i], time[k1],
            synthetic_i[k1], nc1[l1], nc2[l1], nc3[l1], adt[m1], adti[m1], ads[m1], adsi[m1],
            index_comp[i], ihb[i])
        print(i)

        for j in range(m1, m1+double_n-1):
            adt[j] *= plotting_scale[i]
            adti[j] *= plotting_scale[i]
            ads[j] *= plotting_scale[i]
            adsi[j] *= plotting_scale[i]
        
        for k in range(k1, k1+ns[i]-1):
            synthetic[j] = (synthetic[j]+synthetic_i[j])*plotting_scale[i]
        
        l2 = l1+mxy-1
        k1 -= 1
        m1 -= 1
        for j in range(l1,l2):
            n1[j] += k1
            n3[j] += m1
        
        k1 = k1+1+ns[i]
        l1 = l2+1
        m1 = m1+1+double_n

    k1 -= 1
    l1 -= 1
    m1 -= 1
    print(k1 l1 m1)

    ## Preparing the model covariance matrix
    with open("for060.dat", "w") as f_060:
        ainv = 0
        data = 0
        epsi[0] = 0
        for i in range(area_xy):
            d[i] = 0
            im = i+area_xy
            d[im] = 0
            k1 = i
            for k in range(i1):
                for l in range(n3[k1], n3[k1]+n2[k1]-1):
                    d[i] += ads[l]**2
                    d[im] += adt[l]**2
                k1 += area_xy
            
            if ainv < d[i]:
                ainv = d[i]
                f_060.write(i, ainv)
            elif i == imin:
                f_060.write('***' + i +' ' + ainv)
            
            d[i] = np.sqrt(d[i])
            data += d[i]
            d[im] = np.sqrt(d[im])
            epsi[0] += d[im]
        
        data = data/area_xy
        epsi[0] = epsi[0]/area_xy
        for i in range(area_xy2):
            d[i] = 1
        for i in range(gridnum_length):
            d[i] = 0
            d[area_xy-i+1] = 0
            d[ij+area_xy+i] = 1
            ij += gridnum_length
            d[ij] = 0
            d[ij+area_xy] = 0
        mmxy = area_xy*(area_xy+1)/2
        mmxy2 = area_xy2*(area_xy2+1)/2
        
        covariance(ar, mmxy, displacement, area_xy2, gridnum_width, gridnum_length,
            dcor_width, dcor_length)
        
        ## Estimate the relative noise variance for each seismogram
        m1 = 1
        l1 = 0
        for double_i in range(i1):
            ### calculate matrix A=G*Rm*Trans(G)
            m2 = m1 + area_xy -1
            k1 = area_xy2*ns(double_i)
            for i in range(k1):
                a[i] = 0
            j1 = 0
            j2 = area_xy
            ni = 0
            for i in range(m1, m2):
                ni += 1
                k1 = n3[i]
                l2 = n1[i]-1
                jk = area_xy2*(l2-1)
                for k in range(l2, l2+n2[i]-1):
                    ij = j1
                    for j in range(1, ni):
                        ij += 1
                        jk += 1
                        a[jk] += ar[ij]*ads[k1]
                    
                    for j in range(ni+1, area_xy):
                        ij = ij+j-1
                        jk += 1
                        a[jk] += ar[ij]*ads[k1]
                    
                    jk -= area_xy
                    ij = j2
                    for j in range(ni+area_xy):
                        ij += 1
                        jk += 1
                        a[jk] += ar[ij]*adt[k1]
                    
                    for j in range(ni+area_xy+1, area_xy2):
                        ij = ij+j-1
                        jk += 1
                        a[jk] += ar[ij]*adt[k1]
                    k1 += 1

                j1 += ni
                j2 = j2+ni+area_xy

            for i in range(ns[double_i]*(ns[double_i]+1)/2):
                b[i] = 0
            ni = 0
            for i in range(m1, m2):
                ni += 1
                l2 = n1[i] - l1
                l = l2*(l2+1)/2
                k1 = n3[i]
                for i in range(l2, l2+n2[i]+1):
                    jk = j1
                    ij = l
                    for k in range(j, ns[double_i]):
                        jk += area_xy2
                        b[ij] = b[ij] + ads[k1]*a[jk] + adt[k1]*a[jk+area_xy]
                        ij += k
                    k1 += 1
                    l = l + j + 1
            ij = 0
            for i in range(ns[double_i]):
                jk = 0
                for j in range(ns[double_i]):
                    x1[j] = 0
                    k1 = jk
                    for k in range(j):
                        x1[j] += wavelet[i-k]*b[k1]
                    for k in range(j+1, ns[double_i]):
                        k1 = k1 + k -1
                        x1[j] += wavelet[i-k]*b[k1]
                    jk += j
                for j in range(i):
                    ij += 1
                    a[ij] = 0
                    for k in range(ns[double_i]):
                        a[ij] += x1[k]*wavelet[j-k]
            
            ## Calculate the maximum eigenvalue of matrix A
            data = 0
            for i in range(ns[double_i]):
                x1[i] = 1
            for i in range(1000):
                epsi[double_i] = 0
                for j in range(ns[double_i]):
                    x2[j] = 0
                    k1 = j*(j-1)/2
                    for k in range(j):
                        k1 += 1
                        x2[j] += x1[k]*a[k1]
                    for k in range(j+1, ns[double_i]):
                        k1 = k1 + k + 1
                        x2[j] += x1[k]*a[k1]
                    epsi[double_i] += x2[j]**2
                epsi[double_i] = np.sqrt(epsi[double_i])
                if np.absolute(data-epsi[double_i])/epsi[double_i] < err:
                    f_060.write(f'max eigenvalue: ', epsi[double_i])
                    epsi[double_i] *= rns
                    m1 = m2 + 1
                    l1 += ns[double_i]
                data = epsi[double_i]
                f_060.write(epsi[double_i])
                for j in range(ns[double_i]):
                    x1[j] = x2[j]/epsi[double_i]
            f_060.write(f'max eigenvalue: ', epsi[double_i])
            epsi[double_i] *= rns
            m1 = m2 + 1
            l1 += ns[double_i]
        ## Recursive mean square estimation for each additional seismogram
        for i in range(area_xy2):
            d[i] = 0

        ## Recursive estivamtion for each data point
        m1 = 1
        l1 = 0
        with open("temp.dat", "w") as f_tmp:
            for double_i in range(i1):
                f_tmp.write(double_i)
                print(double_i)
                m2 = m1 + area_xy - 1
                for i in range(l1+1, l1+ns(double_i)):
                    for j in range(area_xy2):
                        d1[j] = 0
                
                    if ihb[double_i] == 1:
                        for j in range(m1, m2):
                            jk = j-m1+1
                            dh[jk] = 0
                            im = jk+area_xy
                            dh[im] = 0
                            l = i-n1[j]
                            for k in range(n2[j]-1):
                                dh[jk] = dh[jk] + wavelet[l-k]*ads[n3[j]+k]
                                    + wavelet[l-k]*adsi[n3[j]+k]
                                dh[im] = dh[im] +  wavelet[l-k]*adt[n3[j]+k]
                                    + wavelet[l-k]*adti[n3[j]+k]
                    else:
                        for j in range(m1, m2):
                            jk = j-m1+1
                            dh[jk] = 0
                            im = jk+area_xy
                           dh[im] = 0
                            l = i-n1[j]
                            for k in range(n2[j]-1):
                                dh[jk] += wavelet[l-k]*ads[n3[j]+k]
                                dh[im] += wavelet[l-k]*adt[n3[j]+k]
                
                    j1 = 0
                    for j in range(area_xy2):
                        ij = j1
                        for k in range(j):
                            ij += 1
                            d1[j] += ar[ij]*dh[k]
                        j1 += j

                    ainv = epsi[double_i]
                    data = obs_seis[i] - synthetic[i]
                    for j in range(area_xy2):
                        ainv += dh[j]*d1[j]
                        data -= dh[j]*d[j]
                    ainv = 1/ainv
                    for j in range(area_xy2):
                        ak[j] = ainv*d1[j]
                
                    ### Update the model perturbation and model covariance matrix
                    ij = 0
                    for j in range(area_xy2):
                        for k in range(j):
                            ij += 1
                            ar[ij] -= ak[k]*d1[j]
                        d[j] += data*ak[j]
                    f_tmp.write(ak[j])
                m1 = m2 + 1
                l1 += ns[double_i]
            f_tmp.close()
        for i in range(area_xy):
            f_060.write(d[i])
        for i in range(area_xy):
            slip1_data[i] += d[i]
            slip2_data[i] += d[i+area_xy]
            if slip1_data[i] < 0:
                slip1_data[i] = 0
            if slip2_data[i] < 0:
                slip2_data[i] = 0
            distance = np.sqrt(slip1_data[i]**2 + slip2_data[i]**2)
            if distance >= 1700:
                slip1_data[i] -= d[i]
                slip2_data[i] -= d[i+area_xy]
        
        ## Dump the inversion results
        with open("invo1.data", "w") as f_invo:
            m1 = 1
            for i in range(gridnum_width):
                m2 = m1 + gridnum_length - 1
                for j in range(m1, m2):
                    f_invo.write(rupture_time[j])
                m1 = m2 + 1
            m1 = 1
            for i in range(gridnum_width):
                m2 = m1 + gridnum_length - 1
                for j in range(m1, m2):
                    f_invo.write(slip1_data[j])
                m1 = m2 + 1

            m1 = 1
            for i in range(gridnum_width):
                m2 = m1 + gridnum_length - 1
                for j in range(m1, m2):
                    f_invo.write(slip2_data[j])
                m1 = m2 + 1
            
            k1 = 1
            with open("syn.dat", "w") as f_synthetic:
                for i in range(i1):
                    for j in range(k1, k1+ns[i]-1):
                        f_synthetic.write(syn[j])
                    k1 += ns[i]
                print("Finish inversion!")
                f_060.close()
                f_synthetic.close()
                f_invo.close()
