
import java.io.*;
import java.util.*;
import java.text.*;


public class seisMod3 
{
	public static class FileMod
	{
		int OverResult;
		float ObsNum;
		double time,ObsWave,SynWave;
		String ReadStr,OrigSite,WaveNum,SiteName;
		String AddWord1="-e.txt";
		String AddWord2="-n.txt";
	}
	
	public static class CalcFitness
	{
		double sum1,sum2,sum3;
		double ObsAmpMa,SynAmpMa;
		double ObsWaveRank[],SynWaveRank[];
		double Fitness()
		{
			double Fitness=(Math.min(ObsAmpMa,SynAmpMa)/
			             Math.max(ObsAmpMa,SynAmpMa))*
		                (sum1/(Math.sqrt(sum2)*Math.sqrt(sum3)));
		    return Fitness;
		}
	}		                                    
	
    public static void main (String[] args) throws Exception
    { 	
        Scanner ScanIn2=new Scanner(args[0]);
        String sc2=ScanIn2.next();
        String strin=new String();//<= just for spacing one row
        
        //read how many strong motion sites
        int loopNum=Integer.parseInt(sc2);
 
        //read original waveform file
        BufferedReader fp2=new BufferedReader
        								(new FileReader("seis3-1.dat"));
        //read synthetical waveform file
        BufferedReader fp3=new BufferedReader(new FileReader("syn.dat"));
        /*creat new file for listing Max. number of every strong motion
          sites  and calculated fitness*/
        BufferedWriter fsum=new BufferedWriter
                    					(new FileWriter("calcSum.txt"));
    	try{        
            String str1,str2;
            //prepare for emerge,split waveform files
            
            int i=0,x,z;
            if (fp2.ready() && fp3.ready())
            {
                for(x=0;x<loopNum;x++)               
                {
                	FileMod Fmod=new FileMod (); 
                	CalcFitness CF=new CalcFitness();
                	Fmod.OverResult=x%2;
                    Fmod.ReadStr=fp2.readLine();
                    Fmod.ObsNum=
                         Float.parseFloat(Fmod.ReadStr.substring(0,3));
                    
                    Fmod.OrigSite=Fmod.ReadStr.substring(4,10);
                    if (Fmod.OverResult!=1)
                    {
                    	Fmod.SiteName=Fmod.OrigSite.concat(Fmod.AddWord1);
                    }
                    else
                    {
                    	Fmod.SiteName=Fmod.OrigSite.concat(Fmod.AddWord2);
                    }
                    BufferedWriter fin=new BufferedWriter
                    					(new FileWriter(Fmod.SiteName));
                    
					z=(int)Fmod.ObsNum;
                    CF.ObsWaveRank=new double[z];
                    CF.SynWaveRank=new double[z];
					
					//reading observe and synthetic waveform files
                    for (i=0;i<Fmod.ObsNum;i++)
                    { 
                        str1=fp2.readLine();
        	   	        str2=fp3.readLine();
        	   	        
        	            Fmod.time=
        	                 Double.parseDouble(str1.substring(0,12));
                        Fmod.ObsWave=
                             Double.parseDouble(str1.substring(14,26));
                        Fmod.SynWave=Double.parseDouble(str2);
                        CF.ObsWaveRank[i]=Fmod.ObsWave;
                        CF.SynWaveRank[i]=Fmod.SynWave;

                        //preparing for calculating fitness
                        CF.sum1=CF.sum1+Fmod.ObsWave*Fmod.SynWave;
                        CF.sum2=CF.sum2+Math.pow(Fmod.ObsWave,2);
                        CF.sum3=CF.sum3+Math.pow(Fmod.SynWave,2);
                       	String PrintScr=
                           String.format("%3d %10.7f %10.7f %10.7f%n"
                      			,i,Fmod.time,Fmod.ObsWave,Fmod.SynWave);
                       	String Waveform=
                       	    String.format("%10.7f  %10.7f  %10.7f%n"
                                ,Fmod.time,Fmod.ObsWave,Fmod.SynWave);
						
                       	System.out.print(PrintScr);
                       	System.out.println(strin);
                        
       	            	fin.write(Waveform);
       	            	/*String s4=String.format("%6.3f %6.3f %6.3f%n",
                    		CF.sum1,CF.sum2,CF.sum3);
                    	fin.write(s4);*/
                    }
                    fin.close();
                    String MaxMinFit=new String();
                    Arrays.sort(CF.ObsWaveRank);
                    Arrays.sort(CF.SynWaveRank);
                    CF.ObsAmpMa=CF.ObsWaveRank[CF.ObsWaveRank.length-1];
                    CF.SynAmpMa=CF.SynWaveRank[CF.SynWaveRank.length-1];

                    //starting calculating waveform fitness
                    MaxMinFit=String.format("%6.3f %6.3f %6.3f%n"
                            ,CF.ObsAmpMa,CF.SynAmpMa,CF.Fitness());
                    System.out.println(MaxMinFit);
        	        fsum.write(MaxMinFit);            
    		    }
    		 fsum.close();
            }
        }
        catch (IOException e)
        {
    	    System.out.println("Exception is "+e);
        }
    fp2.close();
    fp3.close();
    }
}
