# Ths function for data processing the file named 1-80.txt and  split into 2 files named "1-80.out" && "1-80.dat"
#
#

import os
import sys
import numpy as np


z = np.zeros((16, 40))
ang = np.zeros((40, 32))
slip = np.zeros((40, 32))
angle = np.zeros((32,32))
array1 = np.zeros((256,4))
data1 = data2 = data3 = data4 = np.zeros((16,16))
my = int(16)
mx = int(16)
maxa = int(1)
angle = '0'
length = int(30)
depth = int(30)
depth1 = int(2)
depth2 = int(32)
length_unit = length/(mx-1)
depth_unit = (depth2-depth1)/(my-1)
# Chek if some file exist
if os.path.isfile('1-80.out'):
	print("Remove the file 1-80.out\n")
	os.remove("1-80.out")
else:
	print("The file doesn't exist\n")

if os.path.isfile('1-80.dat'):
	print("Remove the file 1-80.dat\n")
	os.remove("1-80.dat")
else:
	print("The file doesn't exist\n")


#Read the original file (original 1-80.txt)
array = np.loadtxt('1-80')
rarray = array[...,::-1]
np.set_printoptions(suppress=True)
#print(rarray)
for k in range(mx):
	for i in range(mx):
        	for j in range(my * 2):
				rarray[i][j]
#       print(rarray[i][j])

# Calculation for slip and angle
	            if rarray[i][j] % 2 == 1:
        	    	slip[k][i] = rarray[i][j]
				if slip[k][i] >= maxa:
					maxa = slip[k][i]
	            else:
        	    	ang[k][i] = slip[k][i]

# Before write to the file, prepare some calculation for rupture plane's slip/angle
		data1 = ((i + 1 - 1) * length_unit)
		data2 = ((j + 1 - 1) * depth_unit - depth2)
		data3 = (slip[k][i] * 1.5) 
		data4 = (data3 / (maxa * 2.54))

# Save the values to the specific files
		print('%7.2f %7.2f %7.2f %7.2f %7.4f %7.2f' %(ang[k][i], data1, data2, data3, data4, maxa))
		with open('1-80.out', 'a+') as fout1:
			fout1.write("%7.2f %7.2f %7.2f %7.2f\n" %(data1,  data2, ang[k][i],  data3))
		fout1.close()
		with open('1-80.dat', 'a+') as fout2:
			fout2.write("%7.2f %7.2f %7.2f\n" %(data1,  data2, data4))
		fout2.close()
