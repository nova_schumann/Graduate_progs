# A script calculation seismic moment and epicenter and output data for drawing
import numpy as np

PI = 4*np.arctan(1)

def read_invslp_input(filename):
    with open(filename, 'r') as invslp_file:
        for i in range(3):
            line = invslp_file.readline()
        ## Read  how many layers of velocity structure
        num = int(invslp_file.readline())
        ## Read the velocity structure's data
        for j in range(num):
            layer = invslp_file.readline()
        ## Read the lengh and width of the rupture plane
        length_width = list(invslp_file.readline())
        return length_width
    invslp_file.close()

def basic_fault_data(filename):
    with open(filename, 'r') as invslp_file:
        for i in range(2):
            line = invslp_file.readline()
        ## Read the fault parameters
        third_line = list(invslp_file.readline())
        fault_data = third_line[0:6]
        return fault_data
    invslp_file.close()
        
        


def read_invo_data(invo_file):
    with open(invo_file, 'r') as f_invo:
        
        invslp_input = read_invslp_input("invslp.in")")
        x_direction_data = invslp_input[0]
        y_direction_data = invslp_input[1]
        inversion_data = []
        with open("time", "w") as time_file:
            for i in range(x_direction_data):
                total_data = []
                for j in range(2):
                    data = f_invo.readline().split()
                    total_data = total_data + data
                time_file.write(total_data)
            inversion_data.append(total_data)
        time_file.close()

        '''
        Recording the data along strike/dip of the rupture plane
        '''
        along_dip_data = []
        dip_group_data = []
        along_strike_data = []
        strike_group_data = []
        for k in range(x_direction_data):
            for l in range(2):
                dip_data = f_invo.readline().split()
                along_dip_data = along_dip_data + dip_data
            dip_group_data.append(along_dip_data)
        for m in range(x_direction_data):
            for n in range(2):
                strike_data = f_invo.readline().split()
                along_strike_data = along_strike_data + strike_data
            strike_group_data.append(along_strike_data)
        with open('1-80', 'w') as slip_file:
            for i in range(y_direction_data):
                for j in range(x_direction_data):
                    displacement_offset[j] = np.sqrt(dip_group_data[i][j]**2 + 
                        strike_group_data[i][j]**2)
                    slip_angle = []
                    match slip_angle[j]:
                        case dip_group_data[i][j] == 0 and 
                            strike_group_data[i][j] ==0:
                            slip_angle[j] == 0
                        case dip_group_data[i][j] > 0 and 
                            strike_group_data[i][j] == 0:
                            slip_angle[j] == 0
                        case dip_group_data[i][j] == 0 and 
                            strike_group_data[i][j] < 0:
                            slip_angle[j] == 180
                        case dip_group_data[i][j] == 0 and 
                            strike_group_data[i][j] > 0:
                            slip_angle[j] == 90
                        case dip_group_data[i][j] == 0 and 
                            strike_group_data[i][j] < 0:
                            slip_angle[j] == 270
                        case dip_group_data[i][j] != 0 and 
                            strike_group_data[i][j] != 0:
                            slip_angle[j] = np.arctan(
                                strike_group_data[i][j]/dip_group_data[i][j])*180/PI
                                if dip_group_data[i][j] < 0 and
                                    strike_group_data[i][j] < 0:
                                    slip_angle[j] += 180
                            if dip_group_data[i][j] < 0 and
                                strike_group_data[i][j] > 0:
                                slip_angle[j] += 180
                    slip_file.write(displacement_offset[j], slip_angle[j])
                    average_dislocation = 0
                    average_dislocation += displacement_offset
            
            fault_data = basic_fault_data("invslp.in")
            fault_length = fault_data[0]
            fault_width1 = fault_data[1]
            fault_width2 = fault_data[2]
            epicenter_x = fault_data[3]
            epicenter_y = fault_data[4]
            fault_width = fault_width2 - fault_width1
            rupture_area = fault_length*fault_width
            '''
            Calculation of Seismic moment
            M0 = u * D * S
            u : shear modulus 
            D : average dislocation
            S : rupture area
            '''
            seismic_moment = 3*10**9*rupture_area*10**6*average_dislocation/100
            print(f'Seismic moment = {seismic_moment}, x, y of the epicenter is 
                {epicenter_x}, {epicenter_y}')
        slip_file.close()
        f_invo.close()

if __name__ == __main__:
    read_invo_data("invo.data")
